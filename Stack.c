#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>


// A structure to represent a stack
struct StackNode
{
    int cluster_number;
    char dir_name[12];
    struct StackNode* left;
    struct StackNode* right;
};
 
struct StackNode* newNode(int cluster_number, char * name)
{
    struct StackNode* stackNode = (struct StackNode*) malloc(sizeof(struct StackNode));
    stackNode->cluster_number = cluster_number;
    stackNode->left = NULL;
    stackNode->right = NULL;
    strcpy(stackNode->dir_name, name);
    return stackNode;
}

void push(struct StackNode** head, int cluster_number, char * name)
{
    struct StackNode* stackNode = newNode(cluster_number, name);
    stackNode->left = *head;
    (*head)->right = stackNode;
    *head = stackNode;
}
 
void pop(struct StackNode** head)
{
    if ((*head)->left == NULL)
        return;
    struct StackNode* temp = *head;
    *head = (*head)->left;
    (*head)->right = NULL;
    int popped = temp->cluster_number;
    free(temp);
 
    return;
}

void freeStack(struct StackNode* root, struct StackNode* head){
    
    while(head != root){
        
        pop(&head);
        
    }
    
    pop(&root);
    root = 0;
    
}

