//
//  shell_fat32.c
//  
//
//  Created by Doug Ott II on 4/23/18.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <stdint.h>
#include "FAT32BootBlock.c"
#include "FAT32DirEntry.c"
#include "FAT32DirEntryLong.c"
#include "Stack.c"
#include "openFileTable.c"
#include <sys/wait.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/time.h>

#define INPUT_LIMIT 510

// booleans
#define TRUE 1
#define FALSE 0

/* -Shell stuff- */
void shell_loop(void);      // Main loop
char * shell_read_line(void);   // reads input
char ** shell_split_line(char * line);  // pareses and tokenizes input
int shell_execute(char ** args);    // executes commands
/* -Endo of Shell stuff- */

#define FILE_NAME "fat32.img"

int checkFileOpen(FILE*);

void setFirstSectorOfCluster(FAT32BootBlock*, unsigned int, unsigned int, unsigned int *);

int getBlockInfo (FILE*, FAT32BootBlock*);

int readFAT (FILE* in, FAT32BootBlock*, unsigned int*);

int processDirName(char*);	//caller must provide 13 byte array

int checkProcessDirName(char*);

int readRootClus (FILE*, FAT32BootBlock*, FAT32DirEntry*, FAT32DirEntryLong*, unsigned int firstSectRoot);

void doInfo(FAT32BootBlock*);

void ls(FILE*, FAT32BootBlock*, unsigned int*, unsigned int, struct StackNode*);

void lsDir(char* dirName);

void cd(FILE*, FAT32BootBlock*, unsigned int*, unsigned int,  struct StackNode**, char*);

void doSize(FILE*, FAT32BootBlock*, unsigned int*, unsigned int, struct StackNode*, char*);

int find_empty_cluster();

int mkdir(char * dirName);

int creat(char * dirName);

char* trimStr (int n1, int n2, char* line);

char* insertChar(int index, char c, char* line);

void cpyStr(char* dest, char* source, int start, int end);

char** parseArgs(char* line);

char* parseWhitespace(char* line);

void rmDir(char* dirName);

void rmFile(char* dirName);

//variables
FILE * in;
FILE* out;
FAT32BootBlock* bb;
unsigned int firstDataSector;
unsigned int firstSectRoot;
unsigned int* fATTable;
unsigned int rootClusterNum;
struct StackNode* head;
struct StackNode* rootNode;
struct openFileNode* openFileHead;
struct openFileNode* openFileRoot;

int cmdWordCount = 0;	//parser wordCount

int main() {
	
	in = fopen(FILE_NAME, "rb");
	
	//check if Image opened
	if (!checkFileOpen(in)) {
		
		return 1;
		
	}
	
	//init bb
	bb = (FAT32BootBlock*)calloc(1, sizeof(FAT32BootBlock));
	
	//info()
	getBlockInfo(in, bb);
	rootClusterNum = bb->bpb_rootcluster;
	
	head = newNode(rootClusterNum, ""); // this is root ie first node for pwd tracking
	rootNode = head;

    openFileHead = newOpenFileNode('x',2,0); // makes the head and root for the open file table
	openFileRoot = openFileHead;
	
	//inside loop
	//doInfo(bb);
	
	fATTable = (unsigned int *)calloc( (bb->bpb_FATz32*512/4), sizeof(unsigned int) );
	
	readFAT(in, bb, fATTable);
	
	firstDataSector = bb->reserved_sectors + (bb->number_of_fats * bb->bpb_FATz32);
	setFirstSectorOfCluster(bb, firstDataSector, rootClusterNum, &firstSectRoot);
	
	//TODO: inside loop
	//ls(in, bb, fATTable, firstDataSector, head);
	
	//TODO: inside loop
	int tempClusterNum = rootClusterNum;
	char tempName[13];
	
	strcpy(tempName, "ReD.");
	
	//cd(in, bb, fATTable, firstDataSector, &head , tempName);
	
	//ls(in, bb, fATTable, firstDataSector, head);
	
	strcpy(tempName, "f10");
	
	//TODO: inside loop
	//doSize(in, bb, fATTable, firstDataSector, head, tempName);
	shell_loop();
	
    freeOpenFileTable(&openFileHead, openFileRoot);
	free(fATTable);
	free(bb);
	fclose(in);
	return 0;
}

int checkFileOpen(FILE* in){
	
	if (in == NULL){
		
		printf("\nERROR: Could not open image.\n");
		return 0;
		
	}else{
		
		printf("\nImage opened.\n");
		return 1;
		
	}
	
}

void setFirstSectorOfCluster(FAT32BootBlock* bb, unsigned int firstDataSector, unsigned int n, unsigned int* firstSectorOfCluster){
	
	*firstSectorOfCluster = ((n-2)*bb->sectors_per_cluster) + firstDataSector;
	
}

int getBlockInfo(FILE* in, FAT32BootBlock* bb){
	
	fseek(in, 0, SEEK_SET);
	
	if (fread(bb, sizeof(FAT32BootBlock), 1, in) ){
		
		//read
		
	} else {
		
		printf("Error: Block metadata could not be read.");
		
	}
	
	
	
}

int readFAT(FILE* in, FAT32BootBlock* bb, unsigned int* fATTable){
	
	int numReserved = bb->reserved_sectors;
	int sectSize = bb->sector_size;
	int numFatEntries = bb->bpb_FATz32*512/4;
	
	fseek(in, numReserved*sectSize, SEEK_SET);
	
	if( fread(fATTable, sizeof(unsigned int), numFatEntries, in ) ){
		
		//printf("0x%08x\n", fATTable[0]);
		
	}else{
		
		printf("Error: could not read FAT table");
		
	}
	
}

int writeFAT(){
	
	int outWasOpen = 0, inWasOpen = 0;
	
	
	
	if(in != NULL){
		
		inWasOpen = 1;
		
		fclose(in);
		in = NULL;
	}
	
	if(out != NULL){
		
		outWasOpen = 1;
		
	}else{
		
		out = fopen(FILE_NAME, "r+b");
		
	}
	
	int numReserved = bb->reserved_sectors;
	int sectSize = bb->sector_size;
	int numFatEntries = bb->bpb_FATz32*512/4;
	
	fseek(out, numReserved*sectSize, SEEK_SET);
	
	unsigned int constFATTable[numFatEntries];
	
	for (int i = 0; i < numFatEntries; i++){
		
		constFATTable[i] = fATTable[i];
		
	}
	
	if( fwrite(constFATTable, sizeof(constFATTable[0]), numFatEntries, out ) ){
		
		//printf("0x%08x\n", fATTable[0]);
		
	}else{
		
		printf("Error: could not write to FAT table");
		
	}
	
	if(inWasOpen){
		
		in = fopen(FILE_NAME, "rb");
	}
	
	if(!outWasOpen){
		
		fclose(out);
		out = NULL;
		
	}
	
}

int processDirName(char* dirName){
	
	int i = 0, dotPos = -1;
	int j = 0, k = 0;
	
	for (i = 0; dirName[i] != '\0'; i++){
		
		if (dirName[i] == '.'){
			
			if (dotPos == -1){
				
				dotPos = i;
				
			}else{
				
				dotPos = -2;
				
			}
			
		}
		
	}
	
	if (dotPos == -2){
		
		if (i != 2){
			
			printf("Error: filename contains too many periods. Please try again.\n");
			return 1;
			
		}
		
	}else if (i == 1 && dirName[0] == '.'){
		
		return 0;
		
	}else if (i > 12){
		
		printf("Error: filename too long. Please try again.");
		return 1;	
		
	}else if (dotPos == -1 && i > 11){
		
		printf("Error: filename too long. Please try again.");
		return 1;
		
	}else if (dotPos == 0){
		
		printf("Error: filename cannot start with /'./'. Please try again.");
		return 1;
		
	}else if (dotPos != -1 && dotPos < i - 4){
		
		printf("Error:extension too long. Please try again.");
		return 1;
		
	}else if (dotPos == -1){
		
		for (j = i; j < 11; j++){
			
			dirName[j] = ' ';
			
		}
		
		dirName[j] = '\0';
		
	} else {
		
		char tempName[13];
		tempName[0] = '\0';
		
		for (j = 0, k = 0; j < dotPos; j++){
			
			tempName[k] = dirName[j];
			k++;
			
		}
		
		j++; // skip dot
		
		while (k <= 7){
			
			tempName[k++] = ' ';
			
		}
		
		while(dirName[j] != '\0' ){
			
			tempName[k++] = dirName[j++];
			
		}
		
		while (k <= 10){
			
			tempName[k++] = ' ';
			
		}
		
		tempName[k] = '\0';
		
		strcpy(dirName, tempName);
		
	}
	
	for (j = 0; j < 11; j++){
		
		dirName[j] = toupper(dirName[j]);
		
	}
	
	dirName[j] = '\0';
	
	
}

int checkProcessDirName(char* dirName){
	
	int i = 0, dotPos = -1;
	int j = 0, k = 0;
	
	for (i = 0; dirName[i] != '\0'; i++){
		
		if (dirName[i] == '.'){
			
			if (dotPos == -1){
				
				dotPos = i;
				
			}else{
				
				dotPos = -2;
				
			}
			
		}
		
		if(dirName[i] < 0x20){
			
			if (i != 0){
				
				printf("Error: Invalid Character in filename");
				return 1;
				
			} else {
				
				if (dirName[i] == 0x05){
					
					dirName[i] = 0xE5;
					
				}
				
			}
			
		}else if (dirName[i] == 0x22 || dirName[i] == 0x2A || dirName[i] == 0x2B 
				|| dirName[i] == 0x2C || dirName[i] == 0x2E || dirName[i] == 0x2F 
				|| dirName[i] == 0x3A || dirName[i] == 0x3B || dirName[i] == 0x3C 
				|| dirName[i] == 0x3D || dirName[i] == 0x3E || dirName[i] == 0x3F 
				|| dirName[i] == 0x5B || dirName[i] == 0x5C || dirName[i] == 0x5D 
				|| dirName[i] == 0x7C){
					
				printf("Error: Invalid Character in filename");
				return 1;
					
				}
		
	}
	
	if (dotPos == -2){
		
		printf("Error: filename contains too many periods. Please try again.");
		return 1;
		
	}else if (i > 12){
		
		printf("Error: filename too long. Please try again.");
		return 1;	
		
	}else if (dotPos == -1 && i > 11){
		
		printf("Error: filename too long. Please try again.");
		return 1;
		
	}else if (dotPos == 0){
		
		printf("Error: filename cannot start with /'./'. Please try again.");
		return 1;
		
	}else if (dotPos != -1 && dotPos < i - 4){
		
		printf("Error:extension too long. Please try again.");
		return 1;
		
	}else if (dotPos == -1){
		
		for (j = i; j < 11; j++){
			
			dirName[j] = ' ';
			
		}
		
		dirName[j] = '\0';
		
	} else {
		
		char tempName[13];
		tempName[0] = '\0';
		
		for (j = 0, k = 0; j < dotPos; j++){
			
			tempName[k] = dirName[j];
			k++;
			
		}
		
		j++; // skip dot
		
		while (k <= 7){
			
			tempName[k++] = ' ';
			
		}
		
		while(dirName[j] != '\0' ){
			
			tempName[k++] = dirName[j++];
			
		}
		
		while (k <= 10){
			
			tempName[k++] = ' ';
			
		}
		
		tempName[k] = '\0';
		
		strcpy(dirName, tempName);
		
	}
	
	for (j = 0; j < 11; j++){
		
		dirName[j] = toupper(dirName[j]);
		
	}
	
	dirName[j] = '\0';
	return 0;
	
}

int readRootClus (FILE* in, FAT32BootBlock* bb, FAT32DirEntry* rootDirEntry, FAT32DirEntryLong* rootLongEntry, unsigned int firstSectRoot){
	
	int sectBytes = (firstSectRoot) * bb->sector_size;
	
	fseek(in, sectBytes, SEEK_SET);
	
	if( fread(rootLongEntry, sizeof(FAT32DirEntryLong), 1, in ) ){
		
		if (fread(rootDirEntry, sizeof(FAT32DirEntry), 1, in)){
			
			printf("\nDir Name: %s", rootDirEntry->dir_name);
			printf("\nDir Attr: %d", rootDirEntry->dir_attr);
			printf("\nDir Size: %d", rootDirEntry->dir_file_size);
		
			int temp = rootDirEntry->dir_fst_clus_HI*256 + rootDirEntry->dir_fst_clus_LO;
			//printf("0x%07x", temp);
			printf("\n0x%08x\n", temp);
			
			
		}
		
	}
	
	unsigned int tempSect;
	setFirstSectorOfCluster(bb, firstSectRoot, 16, &tempSect);
	
	printf("%d", tempSect);
	
	sectBytes = (tempSect) * bb->sector_size;
	
	fseek(in, sectBytes, SEEK_SET);
	
	if( fread(rootLongEntry, sizeof(FAT32DirEntryLong), 1, in ) ){
		
		if (fread(rootDirEntry, sizeof(FAT32DirEntry), 1, in)){
			
			printf("\nDir Name: %s", rootDirEntry->dir_name);
			printf("\nDir Attr: %d", rootDirEntry->dir_attr);
			printf("\nDir Size: %d", rootDirEntry->dir_file_size);
		
			int temp = rootDirEntry->dir_fst_clus_HI*256 + rootDirEntry->dir_fst_clus_LO;
			//printf("0x%07x", temp);
			printf("\n0x%08x", temp);
			
			
		}
		
	}
	
	
}

void doInfo(FAT32BootBlock* bb){
	
	printf("\nSector Size: %hu", bb->sector_size);
	printf("\nSectors Per Cluster: %u", bb->sectors_per_cluster);
	printf("\nReserved Sectors: %hu", bb->reserved_sectors);
	printf("\nNumber of FATs: %u", bb->number_of_fats);
	printf("\nRoot Dir Entries: %hu", bb->root_dir_entries);
	printf("\nTotal Sectors Short: %hu", bb->total_sectors_short);
	printf("\nMedia Descriptor: %u", bb->media_descriptor);
	printf("\nFAT Size Sectors: %hu", bb->fat_size_sectors);
	printf("\nSectors Per Track: %hu", bb->sectors_per_track);
	printf("\nNumber of Heads: %hu", bb->number_of_heads);
	printf("\nHidden Sectors: %u", bb->hidden_sectors);
	printf("\nTotal Sectors Long: %u", bb->total_sectors_long);
	printf("\nbpb_FATz32: %u", bb->bpb_FATz32);
	printf("\nbpb_extflags: %u", bb->bpb_extflags);
	printf("\nbpb_fsver: %u", bb->bpb_fsver);
	printf("\nbpb_rootcluster: %u", bb->bpb_rootcluster);
	printf("\nBoot Sector Signature: %d\n", bb->boot_sector_signature);
	
}

void ls(FILE* in, FAT32BootBlock* bb, unsigned int* fATTable, unsigned int firstDataSector, struct StackNode* head){
	
	int current_cluster_number = head->cluster_number;
	
	int i = 0;
	int dotCount = 0;
	
	int sectBytes = 0;
	
	char tempName[12];
	tempName[0] = '\0';
	
	FAT32DirEntry tempDirEntry;
	FAT32DirEntryLong tempDirEntryLong;
	
	do {
		
		setFirstSectorOfCluster(bb, firstDataSector, current_cluster_number, &sectBytes);
		
		sectBytes *= bb->sector_size;
		
		//printf("%08x", sectBytes);
		
		fseek(in, sectBytes, SEEK_SET);
		
		// i is 16 because 512 / 64 = 16
		for (i = 0; i < 8; i++) {
			//getline
			
			tempName[0] = '\0';
			
			if (dotCount < 2 && head != rootNode){
				
				if (fread(&tempDirEntry, sizeof(FAT32DirEntry), 1, in)){
						
					strcpy(tempName, tempDirEntry.dir_name);
					
				}
				
				dotCount++;
				
			}else{
				
				if( fread(&tempDirEntryLong, sizeof(FAT32DirEntryLong), 1, in ) ){
		
					if (fread(&tempDirEntry, sizeof(FAT32DirEntry), 1, in)){
						
						strcpy(tempName, tempDirEntry.dir_name);
						
					}
					
				}
				
			}
			
			// if getline[0] == 0 then no more entries, return;
			if (tempDirEntry.dir_name[0] == 0){//strcmp(tempName, "\0") == 0){
				
				return;
				
			}else{
				
				//printf("xxxx%d%cxxxx", tempDirEntry.dir_name[1], tempName[0]);
				
				if (tempDirEntry.dir_name[0] == 0xe5){
				
					continue;
					
				}
				
			}
			
			// print getline[0 - 11 byte]
			printf("\n%s\n", tempName);
			
		}
		current_cluster_number = fATTable[current_cluster_number];
	} while (current_cluster_number != 0x0FFFFFF8 && current_cluster_number != 0x0FFFFFFF 
				&& current_cluster_number != 0x00000000);
	
}

void lsDir(char* dirName){
	//printf("%s", dirName);
	
	if(strcmp(dirName, "..") == 0){
		
		if(head->cluster_number == bb->bpb_rootcluster){
			
			ls(in, bb, fATTable, firstDataSector, head);
			return;
			
		}else{
			
			int tempNodeClusNum = head->cluster_number;
			char tempDirName[12];
			strcpy(tempDirName, head->dir_name);
			
			pop(&head);
			
			ls(in, bb, fATTable, firstDataSector, head);
			
			push(&head, tempNodeClusNum, tempDirName);
			
			return;
			
		}
		
	}
	
	cd(in, bb, fATTable, firstDataSector, &head, dirName);
	ls(in, bb, fATTable, firstDataSector, head);
	
	char tempDotDot[13] = "..";
	
	if (strcmp(dirName, ".") != 0){
		
		cd(in, bb, fATTable, firstDataSector, &head, tempDotDot);
		
	}
	
}

void cd(FILE* in, FAT32BootBlock* bb, unsigned int* fATTable, unsigned int firstDataSector, struct StackNode** head, char* dirName){
	
	int current_cluster_number = (*head)->cluster_number;
	
	int i = 0;
	
	int sectBytes = 0;
	
	int flagFoundDir = 0;
	
	char tempName[12];
	tempName[0] = '\0';
	
	//printf("\n%s", dirName);
	
	processDirName(dirName); //dirName MUST BE 13+ char
	
	if (strcmp(dirName, "..") == 0){
		
		pop(head);
		return;
		
	} else if (strcmp(dirName, ".") == 0){
		
		return;
		
	}
	
	//printf("\n%s", dirName);
	
	FAT32DirEntry tempDirEntry;
	FAT32DirEntryLong tempDirEntryLong;
	
	do {
		
		setFirstSectorOfCluster(bb, firstDataSector, current_cluster_number, &sectBytes);
		
		sectBytes *= bb->sector_size;
		
		fseek(in, sectBytes, SEEK_SET);
		
		// i is 16 because 512 / 64 = 16
		for (i = 0; i < 8; i++) {
			//getline
			
			tempName[0] = '\0';
			
			if( fread(&tempDirEntryLong, sizeof(FAT32DirEntryLong), 1, in ) ){
		
				if (fread(&tempDirEntry, sizeof(FAT32DirEntry), 1, in)){
					
					strcpy(tempName, tempDirEntry.dir_name);
					
				}
				
			}
			
			tempName[11] = '\0';
			
			if (strcmp(tempName, dirName) == 0){
				
				flagFoundDir = -1;
				if(tempDirEntry.dir_attr == 16){ //if dir_attr == 0x10 -> means is directory
					//printf("\nwsfefwfwef%s", dirName);
					flagFoundDir = 1;
					//TODO: add nodes of linked list
					//new node
					//node->dirname = dirname
					//node->clusternum = formula thingy
					int tempClusNum = tempDirEntry.dir_fst_clus_HI*256 + tempDirEntry.dir_fst_clus_LO;
					
					push(head, tempClusNum, tempName);
					
					//flagFoundDir = -3;
					break;
					
				}
				
			}
			
			// if getline[0] == 0 then no more entries, return;
			if (strcmp(tempName, "\0") == 0){
				
				flagFoundDir = -2;
				
			} 
			
		}
		current_cluster_number = fATTable[current_cluster_number];
	} while ( (current_cluster_number != 0x0FFFFFF8 && current_cluster_number != 0x0FFFFFFF 
				&& current_cluster_number != 0x00000000) && flagFoundDir != 1 && flagFoundDir > -2 && flagFoundDir != -1);
				
	if (flagFoundDir == 0 || flagFoundDir == -2){
		
		printf("Error: directory not found\n");
		
	}else if (flagFoundDir == -1) {
		
		printf("Error: %s is not a directory\n",  tempName);
		
	}else{
		
		//printf("\n%d  %s", (*head)->cluster_number, (*head)->dir_name);
		//printf("sweggwe");
		
	}
	
}

void doSize(FILE* in, FAT32BootBlock* bb, unsigned int* fATTable, unsigned int firstDataSector, struct StackNode* head, char* dirName){
	
	int current_cluster_number = head->cluster_number;
	
	int i = 0;
	
	int sectBytes = 0;
	
	int flagFoundDir = 0;
	
	char tempName[12];
	tempName[0] = '\0';
	
	//printf("\n%s", dirName);
	
	processDirName(dirName); //dirName MUST BE 13+ char
	
	//printf("\n%s", dirName);
	
	FAT32DirEntry tempDirEntry;
	FAT32DirEntryLong tempDirEntryLong;
	
	do {
		
		setFirstSectorOfCluster(bb, firstDataSector, current_cluster_number, &sectBytes);
		
		sectBytes *= bb->sector_size;
		
		fseek(in, sectBytes, SEEK_SET);
		
		// i is 16 because 512 / 64 = 16
		for (i = 0; i < 8; i++) {
			//getline
			
			tempName[0] = '\0';
			
			if( fread(&tempDirEntryLong, sizeof(FAT32DirEntryLong), 1, in ) ){
		
				if (fread(&tempDirEntry, sizeof(FAT32DirEntry), 1, in)){
					
					strcpy(tempName, tempDirEntry.dir_name);
					
				}
				
			}
			
			tempName[11] = '\0';
			
			
			
			if (strcmp(tempName, dirName) == 0){
				
				flagFoundDir = -1;
				
				if(tempDirEntry.dir_attr != 16){ //if dir_attr == 0x10 -> means is directory
					
					char* parsedName;
		    	
			    	parsedName = (char*)calloc(12, sizeof(char));
			    	
			    	strcpy(parsedName, dirName);
			    	
			    	parsedName = parseWhitespace(parsedName);
			    	
			        printf("File Size of %s is %d\n", parsedName, tempDirEntry.dir_file_size);
			        
			        free(parsedName);
					
					flagFoundDir = 1;
					
					break;
					
				}
				
			}
			
			// if getline[0] == 0 then no more entries, return;
			if (tempDirEntry.dir_name[0] == 0){
				
				flagFoundDir = -2;
				
			} 
			
		}
		current_cluster_number = fATTable[current_cluster_number];
	} while ( (current_cluster_number != 0x0FFFFFF8 && current_cluster_number != 0x0FFFFFFF 
				&& current_cluster_number != 0x00000000) && flagFoundDir != 1 && flagFoundDir != -1 && flagFoundDir != -2);
				
	if (flagFoundDir == -2 || flagFoundDir == 0){
		
		printf("Error: file not found\n");
		
	}else if (flagFoundDir == -1){
		
		printf("Error: is a directory\n");
		
	}else{
		
		//TODO
		
	}
	
}

int mkdir(char * dirName) {
	
	int current_cluster_number = head->cluster_number;
	
	int entryClusNum = -1;
	int dirNameClusNum = -1;
	int dirNameEntryNum = -1;
	
	int i = 0;
	
	int sectBytes = 0;
	
	int flagFoundDir = 0;
	
	char tempName[12];
	tempName[0] = '\0';
	
	//printf("\n%s", dirName);
	
	if (checkProcessDirName(dirName)){
		
		return 1;//invalid name
		
	} //dirName MUST BE 13+ char
	
	if (strcmp(dirName, "..") == 0){
		
		//pop(head);
		//return;
		printf("Error: invalid directory name\n");
		return 1;
		
	} else if (strcmp(dirName, ".") == 0){
		
		//return;
		printf("Error: invalid directory name\n");
		return 1;
	}
	
	//printf("\n%s", dirName);
	
	FAT32DirEntry tempDirEntry;
	FAT32DirEntryLong tempDirEntryLong;
	
	do {
		
		setFirstSectorOfCluster(bb, firstDataSector, current_cluster_number, &sectBytes);
		
		sectBytes *= bb->sector_size;
		
		fseek(in, sectBytes, SEEK_SET);
		
		// i is 16 because 512 / 64 = 16
		for (i = 0; i < 8; i++) {
			//getline
			
			tempName[0] = '\0';
			
			if( fread(&tempDirEntryLong, sizeof(FAT32DirEntryLong), 1, in ) ){
		
				if (fread(&tempDirEntry, sizeof(FAT32DirEntry), 1, in)){
					
					strcpy(tempName, tempDirEntry.dir_name);
					
				}
				
			}
			
			tempName[11] = '\0';
			
			if (strcmp(tempName, dirName) == 0){
				
				flagFoundDir = -1;
				if(tempDirEntry.dir_attr != 16){ //if dir_attr == 0x10 -> means is directory
					//printf("\nwsfefwfwef%s", dirName);
					flagFoundDir = 1;
					break;
					
				}
				
			}
			
			entryClusNum = current_cluster_number;
			dirNameEntryNum = i;
			
			// if getline[0] == 0 then no more entries, return;
			if (tempDirEntry.dir_name[0] == 0 || tempDirEntry.dir_name[0] == 0xe5 ){
				
				
				
				flagFoundDir = -2;
				break;
				
			} 
			
		}
		current_cluster_number = fATTable[current_cluster_number];
	} while ( (current_cluster_number != 0x0FFFFFF8 && current_cluster_number != 0x0FFFFFFF 
				&& current_cluster_number != 0x00000000) && flagFoundDir != 1 
				&& flagFoundDir != -2 && flagFoundDir != -1);
				
	if (flagFoundDir == -1 || flagFoundDir == 1){
		
		printf("Error: file/directory with that name already exists \n");
		return 1;
		
	}else if (flagFoundDir == 0) {		//no space in cluster
		
		//printf("Error: %s is a directory, not a file\n",  tempName);
		int newClusNum = find_empty_cluster();
		
		if (newClusNum == -1){
			
			printf("Error: Could not make directory, FAT drive is full\n");
			return 1;
			
		}//else
		
		fATTable[newClusNum] = (unsigned int)0x0ffffff8;
		//writeFAT();
		printf("%d    %d   ", newClusNum, fATTable[newClusNum]);
		
		fATTable[entryClusNum] = (unsigned int)newClusNum;
		writeFAT();
		printf("%d   %d", entryClusNum, fATTable[entryClusNum]);
		
		entryClusNum = newClusNum;	//current working cluster is the newly created one;
		dirNameEntryNum = 0;		//first entry cuz new cluster
		
	}
	//else foundFlagDir == 0
				
	fclose(in);
	in = NULL;
	
	out = fopen(FILE_NAME, "r+b");
	
	setFirstSectorOfCluster(bb, firstDataSector, entryClusNum, &sectBytes);
		
	sectBytes *= bb->sector_size;
	sectBytes += 64*dirNameEntryNum;
	
	fseek(out, sectBytes, SEEK_SET);
	
	char zeroChar = 0;
	
	//write empty long
	for (int z = 0; z < 32; z++){
			
		fwrite(&zeroChar, sizeof(char), 1, out);
		
	}
	
	FAT32DirEntry newDirEntry;
	
	int newDataCluster = find_empty_cluster();
	
	if (newDataCluster == -1){
		
		printf("Error: could not make directory, FAT image full");
		return 1;
		
	}
	//else
	
	for (int dirNameCounter = 0; dirNameCounter < 11; dirNameCounter++){
		
		newDirEntry.dir_name[dirNameCounter] = dirName[dirNameCounter];
		
	}
	
	newDirEntry.dir_attr = 16;		//is directory
	newDirEntry.dir_nt_res = 0;
	newDirEntry.dir_crt_time_tenth = 0;
	newDirEntry.dir_crt_time = 0;
	newDirEntry.dir_crt_date = 0;
	newDirEntry.dir_lst_acc_date = 0;
	newDirEntry.dir_fst_clus_HI = newDataCluster/256;	// div by 0x100
	newDirEntry.dir_wrt_time = 0;
	newDirEntry.dir_wrt_date = 0;
	newDirEntry.dir_fst_clus_LO = newDataCluster%256;	// % by 0x100
	newDirEntry.dir_file_size = 0;
	
	fwrite(&newDirEntry, sizeof(newDirEntry), 1, out);
	
	//setup new data cluster for . and ..
	setFirstSectorOfCluster(bb, firstDataSector, newDataCluster, &sectBytes);
	sectBytes *= bb->sector_size;
	
	fseek(out, sectBytes, SEEK_SET);
	
	//dot entry
	newDirEntry.dir_name[0] = '.';
	
	for (int dirNameCounter = 1; dirNameCounter < 11; dirNameCounter++){
		
		newDirEntry.dir_name[dirNameCounter] = 0;
		
	}
	
	newDirEntry.dir_fst_clus_HI = newDataCluster/256;
	newDirEntry.dir_fst_clus_LO = newDataCluster%256;
	
	//write dot
	fwrite(&newDirEntry, sizeof(newDirEntry), 1, out);
	
	//dotdot entry
	newDirEntry.dir_name[0] = '.';
	newDirEntry.dir_name[1] = '.';
	
	for (int dirNameCounter = 2; dirNameCounter < 11; dirNameCounter++){
		
		newDirEntry.dir_name[dirNameCounter] = 0;
		
	}
	
	newDirEntry.dir_fst_clus_HI = entryClusNum/256;
	newDirEntry.dir_fst_clus_LO = entryClusNum%256;
	
	//write dot
	fwrite(&newDirEntry, sizeof(newDirEntry), 1, out);
	
	
	fclose(out);
	out == NULL;
	
	in = fopen(FILE_NAME, "rb");
	
}

int creat(char * dirName) {
	
	int current_cluster_number = head->cluster_number;
	
	int entryClusNum = -1;
	int dirNameClusNum = -1;
	int dirNameEntryNum = -1;
	
	int i = 0;
	
	int sectBytes = 0;
	
	int flagFoundDir = 0;
	
	char tempName[12];
	tempName[0] = '\0';
	
	//printf("\n%s", dirName);
	
	if (checkProcessDirName(dirName)){
		
		return 1;//invalid name
		
	} //dirName MUST BE 13+ char
	
	if (strcmp(dirName, "..") == 0){
		
		//pop(head);
		//return;
		printf("Error: invalid directory name\n");
		return 1;
		
	} else if (strcmp(dirName, ".") == 0){
		
		//return;
		printf("Error: invalid directory name\n");
		return 1;
	}
	
	//printf("\n%s", dirName);
	
	FAT32DirEntry tempDirEntry;
	FAT32DirEntryLong tempDirEntryLong;
	
	do {
		
		setFirstSectorOfCluster(bb, firstDataSector, current_cluster_number, &sectBytes);
		
		sectBytes *= bb->sector_size;
		
		fseek(in, sectBytes, SEEK_SET);
		
		// i is 16 because 512 / 64 = 16
		for (i = 0; i < 8; i++) {
			//getline
			
			tempName[0] = '\0';
			
			if( fread(&tempDirEntryLong, sizeof(FAT32DirEntryLong), 1, in ) ){
		
				if (fread(&tempDirEntry, sizeof(FAT32DirEntry), 1, in)){
					
					strcpy(tempName, tempDirEntry.dir_name);
					
				}
				
			}
			
			tempName[11] = '\0';
			
			if (strcmp(tempName, dirName) == 0){
				
				flagFoundDir = -1;
				if(tempDirEntry.dir_attr != 16){ //if dir_attr == 0x10 -> means is directory
					//printf("\nwsfefwfwef%s", dirName);
					flagFoundDir = 1;
					break;
					
				}
				
			}
			
			entryClusNum = current_cluster_number;
			dirNameEntryNum = i;
			
			// if getline[0] == 0 then no more entries, return;
			if (tempDirEntry.dir_name[0] == 0 || tempDirEntry.dir_name[0] == 0xe5 ){
				
				
				
				flagFoundDir = -2;
				break;
				
			} 
			
		}
		current_cluster_number = fATTable[current_cluster_number];
	} while ( (current_cluster_number != 0x0FFFFFF8 && current_cluster_number != 0x0FFFFFFF 
				&& current_cluster_number != 0x00000000) && flagFoundDir != 1 
				&& flagFoundDir != -2 && flagFoundDir != -1);
				
	if (flagFoundDir == -1 || flagFoundDir == 1){
		
		printf("Error: file/directory with that name already exists \n");
		return 1;
		
	}else if (flagFoundDir == 0) {		//no space in cluster
		
		//printf("Error: %s is a directory, not a file\n",  tempName);
		int newClusNum = find_empty_cluster();
		
		if (newClusNum == -1){
			
			printf("Error: Could not make directory, FAT drive is full\n");
			return 1;
			
		}//else
		
		fATTable[newClusNum] = (unsigned int)0x0ffffff8;
		//writeFAT();
		printf("%d    %d   ", newClusNum, fATTable[newClusNum]);
		
		fATTable[entryClusNum] = (unsigned int)newClusNum;
		writeFAT();
		printf("%d   %d", entryClusNum, fATTable[entryClusNum]);
		
		entryClusNum = newClusNum;	//current working cluster is the newly created one;
		dirNameEntryNum = 0;		//first entry cuz new cluster
		
	}
	//else foundFlagDir == 0
				
	fclose(in);
	in = NULL;
	
	out = fopen(FILE_NAME, "r+b");
	
	setFirstSectorOfCluster(bb, firstDataSector, entryClusNum, &sectBytes);
		
	sectBytes *= bb->sector_size;
	sectBytes += 64*dirNameEntryNum;
	
	fseek(out, sectBytes, SEEK_SET);
	
	char zeroChar = 0;
	
	//write empty long
	for (int z = 0; z < 32; z++){
			
		fwrite(&zeroChar, sizeof(char), 1, out);
		
	}
	
	FAT32DirEntry newDirEntry;
	
	int newDataCluster = find_empty_cluster();
	
	if (newDataCluster == -1){
		
		printf("Error: could not make directory, FAT image full");
		return 1;
		
	}
	//else
	
	for (int dirNameCounter = 0; dirNameCounter < 11; dirNameCounter++){
		
		newDirEntry.dir_name[dirNameCounter] = dirName[dirNameCounter];
		
	}
	
	newDirEntry.dir_attr = 0;		//is directory
	newDirEntry.dir_nt_res = 0;
	newDirEntry.dir_crt_time_tenth = 0;
	newDirEntry.dir_crt_time = 0;
	newDirEntry.dir_crt_date = 0;
	newDirEntry.dir_lst_acc_date = 0;
	newDirEntry.dir_fst_clus_HI = newDataCluster/256;	// div by 0x100
	newDirEntry.dir_wrt_time = 0;
	newDirEntry.dir_wrt_date = 0;
	newDirEntry.dir_fst_clus_LO = newDataCluster%256;	// % by 0x100
	newDirEntry.dir_file_size = 0;
	
	fwrite(&newDirEntry, sizeof(newDirEntry), 1, out);
	
	//setup new data cluster for . and ..
	setFirstSectorOfCluster(bb, firstDataSector, newDataCluster, &sectBytes);
	sectBytes *= bb->sector_size;
	
	fseek(out, sectBytes, SEEK_SET);
	
	fclose(out);
	out == NULL;
	
	in = fopen(FILE_NAME, "rb");
	
}

int find_empty_cluster() {
	
	int numFatEntries = bb->bpb_FATz32*512/4;
	
	for(int j = 2; j < numFatEntries; j++){
		
		if (fATTable[j] == 0){
			
			return j;
			
		}
		
	}
	//else
	
	return -1;

}

// Function that opens files
void openFile(char * filename, char * mode){

	int current_cluster_number = head->cluster_number;	
	int i = 0;
	int dotCount = 0;
	int sectBytes = 0;
	char tempName[12];
	tempName[0] = '\0';
    processDirName(filename);
	
	FAT32DirEntry tempDirEntry;
	FAT32DirEntryLong tempDirEntryLong;
	
	do {
		setFirstSectorOfCluster(bb, firstDataSector, current_cluster_number, &sectBytes);
		sectBytes *= bb->sector_size;
		fseek(in, sectBytes, SEEK_SET);
		
		// i is 16 because 512 / 64 = 16
		for (i = 0; i < 8; i++) {
			//getline
			tempName[0] = '\0';
			if (dotCount < 2 && head != rootNode){	
				if (fread(&tempDirEntry, sizeof(FAT32DirEntry), 1, in)){		
					strcpy(tempName, tempDirEntry.dir_name);	
				}
				dotCount++;	
			}else{
				if( fread(&tempDirEntryLong, sizeof(FAT32DirEntryLong), 1, in ) ){
					if (fread(&tempDirEntry, sizeof(FAT32DirEntry), 1, in)){
						strcpy(tempName, tempDirEntry.dir_name);
					}
				}
			}
			
			// if getline[0] == 0 then no more entries, return;
			if (strcmp(tempName, "\0") == 0) {
                printf("Error: No file found with name %s\n", filename);
				return;	
            }
			
            int ghj;
            int boolSameVal = 0;
            for (ghj = 0; ghj < 11; ghj++){
                if (tempName[ghj] != filename[ghj]) 
                    boolSameVal = 1;
            }

            /* --- THIS IS WHERE OPEN REALLY STARTS --- */
            // everything up to here was just to find the file

            // if a file is found that matches the filename
            if (boolSameVal == 0) {
                int tempInt = (int) tempDirEntry.dir_attr;
                //printf("Directory found: attribute is = %d\n", tempInt);
                if ((strcmp(mode, "") == 0)) {
                    printf("Error: mode is not given\n");
                    return;
                }
                if (tempInt == 16) {
                    printf("Error: Directory is a folder: %s\n", filename);
                }
                else {
                    if ((strcmp(mode, "rw") == 0) || (strcmp(mode, "wr") == 0)) {
                        if (tempInt == 1) {
                            printf("Error: file access is read only\n");
                        }
                        else {
                            
                            if (searchOpenFileTable(&openFileHead, tempDirEntry.dir_fst_clus_LO, tempDirEntry.dir_fst_clus_HI)) {
                                printf("Error: File is already opened\n");
                            }
                            else {
                                pushOpenFileNode(&openFileHead, 'x', tempDirEntry.dir_fst_clus_LO, tempDirEntry.dir_fst_clus_HI);
                                //printOpenFileTable(openFileRoot);
                            }
                        }
                    }
                    else if ((strcmp(mode, "r") == 0)) {
                        if (searchOpenFileTable(&openFileHead, tempDirEntry.dir_fst_clus_LO, tempDirEntry.dir_fst_clus_HI)) {
                            printf("Error: File is already opened\n");
                        }
                        else {
                            pushOpenFileNode(&openFileHead, 'r', tempDirEntry.dir_fst_clus_LO, tempDirEntry.dir_fst_clus_HI);
                            //printOpenFileTable(openFileRoot);
                        }
                    }
                    else if ((strcmp(mode, "w") == 0)) {
                        if (tempInt == 1) {
                            printf("Error: file access is read only\n");
                        }
                        else {
                            
                            if (searchOpenFileTable(&openFileHead, tempDirEntry.dir_fst_clus_LO, tempDirEntry.dir_fst_clus_HI)) {
                                printf("Error: File is already opened\n");
                            }
                            else {
                                pushOpenFileNode(&openFileHead, 'w', tempDirEntry.dir_fst_clus_LO, tempDirEntry.dir_fst_clus_HI);
                                //printOpenFileTable(openFileRoot);
                            }
                        }
                    }
                    else {
                        printf("Error: Invalid mode, must be one of ['w', 'r', 'wr', 'rw']\n");
                    }
                }
                return;
            } 
		}
		current_cluster_number = fATTable[current_cluster_number];
	} while (current_cluster_number != 0x0FFFFFF8 || current_cluster_number != 0x0FFFFFFF 
				|| current_cluster_number != 0x00000000);	
}

void closeFile(char * filename){

	int current_cluster_number = head->cluster_number;	
	int i = 0;
	int dotCount = 0;
	int sectBytes = 0;
	char tempName[12];
	tempName[0] = '\0';
    processDirName(filename);
	
	FAT32DirEntry tempDirEntry;
	FAT32DirEntryLong tempDirEntryLong;
	
	do {
		setFirstSectorOfCluster(bb, firstDataSector, current_cluster_number, &sectBytes);
		sectBytes *= bb->sector_size;
		fseek(in, sectBytes, SEEK_SET);
		
		// i is 16 because 512 / 64 = 16
		for (i = 0; i < 8; i++) {
			//getline
			tempName[0] = '\0';
			if (dotCount < 2 && head != rootNode){	
				if (fread(&tempDirEntry, sizeof(FAT32DirEntry), 1, in)){		
					strcpy(tempName, tempDirEntry.dir_name);	
				}
				dotCount++;	
			}else{
				if( fread(&tempDirEntryLong, sizeof(FAT32DirEntryLong), 1, in ) ){
					if (fread(&tempDirEntry, sizeof(FAT32DirEntry), 1, in)){
						strcpy(tempName, tempDirEntry.dir_name);
					}
				}
			}
			
			// if getline[0] == 0 then no more entries, return;
			if (strcmp(tempName, "\0") == 0) {
                printf("Error: No file found with name %s\n", filename);
				return;	
            }
			
            int ghj;
            int boolSameVal = 0;
            for (ghj = 0; ghj < 11; ghj++){
                if (tempName[ghj] != filename[ghj]) 
                    boolSameVal = 1;
            }

            /* --- THIS IS WHERE OPEN REALLY STARTS --- */
            // everything up to here was just to find the file

            // if a file is found that matches the filename
            if (boolSameVal == 0) {
				if (searchOpenFileTable(&openFileHead, tempDirEntry.dir_fst_clus_LO, tempDirEntry.dir_fst_clus_HI)) {
					removeOpenFile(&openFileHead, tempDirEntry.dir_fst_clus_LO, tempDirEntry.dir_fst_clus_HI);
					//printOpenFileTable(openFileRoot);
				}
				else {
					printf("Error: File is not opened\n");
				}
                return;
            } 
		}
		current_cluster_number = fATTable[current_cluster_number];
	} while (current_cluster_number != 0x0FFFFFF8 || current_cluster_number != 0x0FFFFFFF 
				|| current_cluster_number != 0x00000000);	
}

// Function that reads a file between its given offset to size, error handling included
void readFile(char * filename, char * offsetString, char * sizeString){

    //printf("%d\n", offset);
    //printf("%d\n", size);

	int current_cluster_number = head->cluster_number;	
	int i = 0;
	int dotCount = 0;
	int sectBytes = 0;
	char tempName[12];
	tempName[0] = '\0';
    processDirName(filename);
	
	FAT32DirEntry tempDirEntry;
	FAT32DirEntryLong tempDirEntryLong;
	do {
		setFirstSectorOfCluster(bb, firstDataSector, current_cluster_number, &sectBytes);
		sectBytes *= bb->sector_size;
		fseek(in, sectBytes, SEEK_SET);
		
		// i is 16 because 512 / 64 = 16
		for (i = 0; i < 8; i++) {
			//getline
			tempName[0] = '\0';
			if (dotCount < 2 && head != rootNode){	
				if (fread(&tempDirEntry, sizeof(FAT32DirEntry), 1, in)){		
					strcpy(tempName, tempDirEntry.dir_name);	
				}
				dotCount++;	
			}else{
				if( fread(&tempDirEntryLong, sizeof(FAT32DirEntryLong), 1, in ) ){
					if (fread(&tempDirEntry, sizeof(FAT32DirEntry), 1, in)){
						strcpy(tempName, tempDirEntry.dir_name);
					}
				}
			}
			
			// if getline[0] == 0 then no more entries, return;
			if (strcmp(tempName, "\0") == 0) {
                printf("Error: No file found with name %s\n", filename);
				return;	
            }
			
            int ghj;
            int boolSameVal = 0;
            for (ghj = 0; ghj < 11; ghj++){
                if (tempName[ghj] != filename[ghj]) 
                    boolSameVal = 1;
            }

            /* --- THIS IS WHERE READ REALLY STARTS --- */
            // everything up to here was just to find the file

            // if a file is found that matches the filename
            if (boolSameVal == 0) {
                int tempInt = (int) tempDirEntry.dir_attr;
                if (tempInt == 16) {
                    printf("Error: Directory is a folder: %s\n", filename);
                }
                else {   
                    int offset = 0;
                    int size = 0;
                    offset = atoi(offsetString);
                    size = atoi(sizeString);

                    if (offset > size) {
                        printf("Error: Offset given is larger than size\n");
                        return;
                    }        

                    if (searchOpenFileTable(&openFileHead, tempDirEntry.dir_fst_clus_LO, tempDirEntry.dir_fst_clus_HI)) {
                        char mode = getOpenFileMode(&openFileHead, tempDirEntry.dir_fst_clus_LO, tempDirEntry.dir_fst_clus_HI);
                        if (mode == 'w') {
                            printf("Error: cannot read file, file is write only\n");
                        }
                        else {
                            // READ FILE
                        }
                    }
                    else {
                        printf("Error: File is not opened\n");
                        //pushOpenFileNode(&openFileHead, 'x', tempDirEntry.dir_fst_clus_LO, tempDirEntry.dir_fst_clus_HI);
                        //printOpenFileTable(openFileRoot);
                    }
                }
                return;
            } 
		}
		current_cluster_number = fATTable[current_cluster_number];
	} while (current_cluster_number != 0x0FFFFFF8 || current_cluster_number != 0x0FFFFFFF 
				|| current_cluster_number != 0x00000000);	
}

// Function that opens files
void writeFile(char * filename, char * offsetString, char * sizeString, char * string){

    int offset = 0;
    int size = 0;
    offset = atoi(offsetString);
    size = atoi(sizeString);

    if (offset > size) {
        printf("Error: Offset given is larger than size\n");
        return;
    }

	int current_cluster_number = head->cluster_number;	
	int i = 0;
	int dotCount = 0;
	int sectBytes = 0;
	char tempName[12];
	tempName[0] = '\0';
    processDirName(filename);
	
	FAT32DirEntry tempDirEntry;
	FAT32DirEntryLong tempDirEntryLong;
	
	do {
		setFirstSectorOfCluster(bb, firstDataSector, current_cluster_number, &sectBytes);
		sectBytes *= bb->sector_size;
		fseek(in, sectBytes, SEEK_SET);
		
		// i is 16 because 512 / 64 = 16
		for (i = 0; i < 8; i++) {
			//getline
			tempName[0] = '\0';
			if (dotCount < 2 && head != rootNode){	
				if (fread(&tempDirEntry, sizeof(FAT32DirEntry), 1, in)){		
					strcpy(tempName, tempDirEntry.dir_name);	
				}
				dotCount++;	
			}else{
				if( fread(&tempDirEntryLong, sizeof(FAT32DirEntryLong), 1, in ) ){
					if (fread(&tempDirEntry, sizeof(FAT32DirEntry), 1, in)){
						strcpy(tempName, tempDirEntry.dir_name);
					}
				}
			}
			
			// if getline[0] == 0 then no more entries, return;
			if (strcmp(tempName, "\0") == 0) {
                printf("Error: No file found with name %s\n", filename);
				return;	
            }
			
            int ghj;
            int boolSameVal = 0;
            for (ghj = 0; ghj < 11; ghj++){
                if (tempName[ghj] != filename[ghj]) 
                    boolSameVal = 1;
            }

            /* --- THIS IS WHERE WRITE REALLY STARTS --- */
            // everything up to here was just to find the file

            // if a file is found that matches the filename
            if (boolSameVal == 0) {
                int tempInt = (int) tempDirEntry.dir_attr;
                if (tempInt == 16) {
                    printf("Error: Directory is a folder: %s\n", filename);
                }
                else {                          
                    if (searchOpenFileTable(&openFileHead, tempDirEntry.dir_fst_clus_LO, tempDirEntry.dir_fst_clus_HI)) {
                        char mode = getOpenFileMode(&openFileHead, tempDirEntry.dir_fst_clus_LO, tempDirEntry.dir_fst_clus_HI);
                        if (mode == 'r') {
                            printf("Error: cannot write file, file is read only\n");
                        }
                        else {
                            // READ FILE
                        }
                    }
                    else {
                        printf("Error: File is not opened\n");
                        //pushOpenFileNode(&openFileHead, 'x', tempDirEntry.dir_fst_clus_LO, tempDirEntry.dir_fst_clus_HI);
                        //printOpenFileTable(openFileRoot);
                    }
                }
                return;
            } 
		}
		current_cluster_number = fATTable[current_cluster_number];
	} while (current_cluster_number != 0x0FFFFFF8 || current_cluster_number != 0x0FFFFFFF 
				|| current_cluster_number != 0x00000000);	
}

/*
 int FAT_writeFatEntry(FAT32BootBlock* bb, uint32_t destinationCluster, uint32_t * newFatVal) {
 FILE* f = fopen("fat32.img", "r+");
 checkFileOpen(f);
 fseek(f, getFatAddressByCluster(bb, destinationCluster), 0);
 fwrite(newFatVal, 4, 1, f);
 fclose(f);
 //if(DEBUG == TRUE) printf("FAT_writeEntry: wrote->%d to cluster %d", *newFatVal, destinationCluster);
 return 0;
 }
 */

/*description: feed this a cluster and it returns an offset, in bytes, where the
 * fat entry for this cluster is locted
 *
 * use: use the result to seek to the place in the image and read
 * the FAT entry
 * */
/*
int getFatAddressByCluster(FAT32BootBlock* bb, uint32_t clusterNum) {
	uint32_t fatOffset = clusterNum * 4;
	uint32_t fatSectorNum = bb->reserved_sectors + (fatOffset / bb->sector_size);
	uint32_t fatEntryOffset = fatOffset % bb->sector_size;
	return (fatSectorNum * bb->sector_size + fatEntryOffset);
	// this function used to return a uint32_t
}
 */
 
char* trimStr (int n1, int n2, char* line){
    
    if (n2==-1){
        
        char* tempLine = line;
    
        line = (char*)calloc(INPUT_LIMIT, sizeof(char));
        
        int i = 0, j = 0;
        
        for (i = 0, j = n1; tempLine[j]!='\0'; i++, j++){
            
            line[i] = tempLine[j];
            
        }
        
        line[i] = '\0';
        
        free(tempLine);
        
        return line;
        
    }else if(n1 == -1){
        
        line[n2] = '\0';
        
        return line;
        
    }else{
        
        char* tempLine = line;
    
        line = (char*)calloc(INPUT_LIMIT, sizeof(char));
        
        int i = 0, j = 0;
        
        for (i = 0, j = 0; j < n1; i++, j++){
            
            line[i] = tempLine[j];
            
        }
        
        j = n2;
        
        for (j = n2; tempLine[j] != '\0'; i++, j++){
            
            line[i] = tempLine[j];
            
        }
        
        line[i] = '\0';
        
        free(tempLine);
        
        return line;
        
    }
    
    
}

char* insertChar(int index, char c, char* line){
    
    char* tempLine = line;
    
    line = (char*)calloc(INPUT_LIMIT, sizeof(char));
    
    int i = 0, j = 0;
    
    for (i = 0, j = 0; j < index; i++, j++){
        
        line[i] = tempLine[j];
        
    }
    
    line[i] = c;
    i++;
    
    for (; tempLine[j] != '\0'; i++, j++){
        
        line[i] = tempLine[j];
        
    }
    
    line[i] = '\0';
    
    free(tempLine);
    
    return line;
    
}
 
void cpyStr(char* dest, char* source, int start, int end){
    
    int i = 0, j = 0;
    
    for(i = 0, j = start; j <= end; i++,j++){
        
        dest[i] = source[j];
        
    }
    
    dest[i] = '\0';
    
}

char** parseArgs(char* line){
    
    int i = 0, tCount = 0, j = 0, k=0;
    
    for(i = 0; line[i] != '\0'; i++){
        
        if(line[i] == ' '){
            
            tCount++;
            
        }
        
    }
    
    cmdWordCount = tCount+1;
    
    int* delimLoc = (int*)calloc(cmdWordCount, sizeof(int));
    
    for(i = 0, j = 0; line[i] != '\0'; i++){
        
        if(line[i] == ' '){
            
           delimLoc[j] = i;
           j++;
            
        }
        
    }
    
    delimLoc[j] = i;
    
    char** tCmd = (char**)calloc(cmdWordCount, sizeof(char*));
    
    for(i = 0; i < cmdWordCount; i++){
        
        tCmd[i] = (char*)calloc(INPUT_LIMIT, sizeof(char));
        
    }
    
    if(cmdWordCount <= 1){
        
        strcpy(tCmd[0], line);
        
    }else{
        
        cpyStr(tCmd[0], line, 0, delimLoc[0]-1);
        
        for(i = 1, j = 0, k = 1; i < cmdWordCount; i++, j++, k++){
        
            cpyStr(tCmd[i], line, delimLoc[j]+1, delimLoc[k]-1);

        }
        
    }
    
    free(delimLoc);
    
    return tCmd;
    
}

char* parseWhitespace(char* line){
    
    int i = 0;
    //trim leading
    for (i = 0; line[i] == ' '; i++);
    
    line = trimStr(i, -1, line);
    
    //trim trailing
    for (i = INPUT_LIMIT-1; line[i] == '\0'; i--);
    
    for (; line[i] == ' '; i--);
    
    line = trimStr(-1, i+1, line);
    
    while(1){
        
        int j = 0, foundSpace = 0, spacePos1 = -1, spacePos2 = -1;
        
        for (j = 0; line[j] != '\0'; j++){
            
            if(line[j] == ' '){
                
                if(spacePos1 == -1){
                    
                    spacePos1 = j;
                    spacePos2 = j;
                    
                }else{
                    
                    foundSpace = 1;
                    spacePos2 = j;
                    
                }
                
            }else{
                
                if(foundSpace){
                    
                    break;
                    
                }else{
                    
                    spacePos1 = -1;
                    spacePos2 = -1;
                    
                }
                
            }
            
        }
        
        if(foundSpace){
            
            line = trimStr(spacePos1, spacePos2, line);
            continue;
            
        }else{
            
            break;
            
        }
        
        
    }
    
    for (i = 0; line[i] != '\0'; i++){
        //if special
        if(line[i] == '|' || line[i] == '<' ||  line[i] == '>'
            || line[i] == '&' /*|| line[i] == '$' || line[i] == '~'*/){
                
                //if not first char 
                if(i != 0){
                    
                    if (line[i-1] != ' '){
                        
                        line = insertChar(i, ' ', line);
                        i++;
                    }
                    
                }
                
                //if not last char
                if (line[i+1] != '0'){
                    
                    if (line[i+1] != ' '){
                        
                        line = insertChar(i+1, ' ', line);
                        //continue;
                    }
                    
                }
                
            }
        
    }
    
    for (i = INPUT_LIMIT-1; line[i] == '\0'; i--);
    
    for (; line[i] == ' '; i--);
    
    line = trimStr(-1, i+1, line);
    
    return line;
    
}

void print(struct StackNode* root) {
    while (root != NULL) {
    	
    	char* tempName;
    	
    	tempName = (char*)calloc(12, sizeof(char));
    	
    	strcpy(tempName, root->dir_name);
    	
    	tempName = parseWhitespace(tempName);
    	
        printf("%s/", tempName );
        
        free(tempName);
        
        root = root->right;
    }
}

// main shell loop
void shell_loop(void)
{
  char * line;
  char ** args;
  int status;
  char * pUser = getenv("USER");
  char * pHost = getenv("HOSTNAME");
  char cDirectory[255];

    do {
        getcwd(cDirectory, sizeof(cDirectory));
        printf("%s/", cDirectory);
		//printf("%s\n", head->dir_name);
		print(rootNode);
		printf(" >> ");
        line = shell_read_line();
        //args = shell_split_line(line);
        line = parseWhitespace(line);
        args = parseArgs(line);
        if (args != NULL) {
            status = shell_execute(args);
        }

        free(line);
        free(args);
    } while (status);
}

// read line from stdin, returns stdin input
char * shell_read_line(void)
{
  int bufsize = 1024;
  int position = 0;
  char * buffer = calloc(bufsize, sizeof(char));
  int c;

    if (!buffer) {
        fprintf(stderr, "shell: error in memory allocation\n");
        exit(EXIT_FAILURE);
    }

    while (1) {
    // Read a character
    c = getchar();

    // If we hit EOF, replace it with a null character and return.
    if (c == EOF || c == '\n') {
        buffer[position] = '\0';
        return buffer;
    }
    else {
            buffer[position] = c;
        }
        position++;

        // If we have exceeded the buffer, reallocate.
        if (position >= bufsize) {
            bufsize += 1024;
            buffer = realloc(buffer, bufsize);
            if (!buffer) {
                fprintf(stderr, "shell: error in memory allocation\n");
                exit(EXIT_FAILURE);
            }
        }
    }
}

// splits input into '\0' terminate token strings, returns tokens ends with NULL
char ** shell_split_line(char * line)
{
    int bufsize = 64, position = 0;
    char ** tokens = calloc(bufsize, sizeof(char*));
    char * token = (char*) calloc(255, sizeof(char));
    int i = 0;
    int j = 0;
    int looper = 1;

    if (!tokens && !token) {
        fprintf(stderr, "Shell: error in memory allocation\n");
        exit(EXIT_FAILURE);
    }

    do {
        if (line[i] == ' ' || line[i] == '\t' || line[i] == '\r' || line[i] == '\a' || line[i] == '\n' || line[i] == '\0') {
            token[j] = '\0';
            tokens[position] = token;
            j = 0;
            token = (char*) calloc(255, sizeof(char));
            if (!token) {
                fprintf(stderr, "Shell: error in memory allocation\n");
                exit(EXIT_FAILURE);
            }
            position++;
            if (position >= bufsize) {
                bufsize += 64;
                tokens = realloc(tokens, bufsize * sizeof(char*));
                if (!tokens) {
                    fprintf(stderr, "Shell: error in memory allocation\n");
                    exit(EXIT_FAILURE);
                }
            }
            if (line[i] == '\0')
                looper = 0;
        }
        else {
            if (line[i] == '|' || line[i] == '<' || line[i] == '>' || line[i] == '&') {
                if (line[i - 1] != ' ') {
                    token[j] = '\0';
                    tokens[position] = token;
                    j = 0;
                    token = (char*) calloc(255, sizeof(char));
                    if (!token) {
                        fprintf(stderr, "Shell: error in memory allocation\n");
                        exit(EXIT_FAILURE);
                    }
                    position++;
                    if (position >= bufsize) {
                        bufsize += 64;
                        tokens = realloc(tokens, bufsize * sizeof(char*));
                        if (!tokens) {
                            fprintf(stderr, "Shell: error in memory allocation\n");
                            exit(EXIT_FAILURE);
                        }
                    }                    
                }
                                    
                token[j] = line[i];
                token[j + 1] = '\0';
                tokens[position] = token;
                j = 0;
                token = (char*) calloc(255, sizeof(char));
                if (!token) {
                    fprintf(stderr, "Shell: error in memory allocation\n");
                    exit(EXIT_FAILURE);
                }
                position++;
                if (position >= bufsize) {
                    bufsize += 64;
                    tokens = realloc(tokens, bufsize * sizeof(char*));
                    if (!tokens) {
                        fprintf(stderr, "Shell: error in memory allocation\n");
                        exit(EXIT_FAILURE);
                    }
                }
                if (line[i + 1] == ' ') {
                    i++;
                }
                if (line[i + 1] == '\0') {
                    looper = 0;
                }
            }
            else {
                token[j] = line[i];
                j++;
            }
        }
        i++;
    } while(looper);

    if (tokens[0][0] == '|' || tokens[0][0] == '<' || tokens[0][0] == '>' || tokens[0][0] == '&') {
        fprintf(stderr, "Shell: improper use of %s\n", tokens[0]);
            free(token);
            tokens = NULL;
            return tokens;
    }

    int synChecker = 0;
    for (int k = 0; k < position; k++) {
        if (tokens[k][0] == '|' || tokens[k][0] == '<' || tokens[k][0] == '>' || tokens[k][0] == '&')   {
            synChecker++;
        }
        else {
            synChecker = 0;
        }
        if (synChecker > 1) {
            fprintf(stderr, "Shell: improper use of %s\n", tokens[k]);
            free(token);
            tokens = NULL;
            return tokens;
        }
    }
    tokens[position] = NULL;

    for (int y = 0; y != position; y++) {
        if (tokens[y][0] == '$') {
            token = (char*) calloc(255, sizeof(char));
            for (int h = 1; tokens[y][h] != '\0'; h++) {
                token[h - 1] = tokens[y][h];
                token[h] = '\0';
            }
            tokens[y] = getenv(token);
        }
    }
    free(token);
    return tokens;
}

// execute shell builtin functions, if not the lanuches standalone applications
int shell_execute(char ** args)
{
    if ((args[0] == NULL) || (strcmp(args[0], "") == 0)) {
        return 1;
	}else{
		
		int foundArg1 = 0, foundArg2 = 0;
		
		if (cmdWordCount == 2){
			
			foundArg1 = 1;
			//printf("%s", args[1]);
		}else if (cmdWordCount == 3){
			
			foundArg1 = 1;
			foundArg2 = 1;
			
		}
		
		if (strcmp(args[0], "exit") == 0) {
			return 0;
		}
		if (strcmp(args[0], "info") == 0) {
			doInfo(bb);
			return 1;
		}
		if (strcmp(args[0], "ls") == 0) {
			
			if (foundArg1){
				
				lsDir(args[1]);
				
			}else{
				
				ls(in, bb, fATTable, firstDataSector, head);
				
			}
			
			return 1;
		}
		if (strcmp(args[0], "cd") == 0) {
			
			if (foundArg1){
				
				char tempName[13];
				strcpy(tempName, args[1]);
				cd(in, bb, fATTable, firstDataSector, &head , tempName);
					
			}else{
				
				printf("Error: Not enough arguments\n");
				
			}
			
			return 1;
		}
		if (strcmp(args[0], "size") == 0) {
			
			if (foundArg1){
				
				doSize(in, bb, fATTable, firstDataSector, head, args[1]);
					
			}else{
				
				printf("Error: Not enough arguments\n");
				
			}
			
			return 1;
		}
		if (strcmp(args[0], "creat") == 0) {
			if (foundArg1){
				
				creat(args[1]);
					
			}else{
				
				printf("Error: Not enough arguments\n");
				
			}
			
			return 1;
		}
		if (strcmp(args[0], "mkdir") == 0) {
			if (foundArg1){
				
				mkdir(args[1]);
					
			}else{
				
				printf("Error: Not enough arguments\n");
				
			}
			
			return 1;
		}
		if (strcmp(args[0], "rm") == 0) {
			
			if (foundArg1){
				
				rmFile(args[1]);
					
			}else{
				
				printf("Error: Not enough arguments\n");
				
			}
			
			return 1;
		}
		if (strcmp(args[0], "rmdir") == 0) {
			
			if (foundArg1){
				
				rmDir(args[1]);
					
			}else{
				
				printf("Error: Not enough arguments\n");
				
			}
			
			return 1;
		}
        if (strcmp(args[0], "open") == 0) {
            if (foundArg1 && foundArg2){
            	
            	openFile(args[1], args[2]);
            	
            }else{
            	
            	printf("Error: incorrect number of arguments\n");
            	
            }
            return 1;
		}
		if (strcmp(args[0], "close") == 0) {
            if (foundArg1){
            	
            	closeFile(args[1]);
            	
            }else{
            	
            	printf("Error: incorrect number of arguments\n");
            	
            }
			return 1;
		}
		if (strcmp(args[0], "read") == 0) {
		    if (cmdWordCount == 4) {
                readFile(args[1], args[2], args[3]);
            }else {
                printf("Error: incorrect number of arguments\n");
            }
            return 1;
		}
		if (strcmp(args[0], "write") == 0) {
			if (cmdWordCount == 5) {
                writeFile(args[1], args[2], args[3], args[4]);
            }else {
                printf("Error: incorrect number of arguments\n");
            }
			return 1;
		}
		printf("Error, no command found: %s\n", args[0]);
		return 1;
		//else
		
	}
	
	
}

void rmDir(char* dirName){
	
	int current_cluster_number = head->cluster_number;
	
	int entryClusNum = -1;
	int dirNameClusNum = -1;
	int dirNameEntryNum = -1;
	
	int i = 0;
	
	int sectBytes = 0;
	
	int flagFoundDir = 0;
	
	char tempName[12];
	tempName[0] = '\0';
	
	//printf("\n%s", dirName);
	
	processDirName(dirName); //dirName MUST BE 13+ char
	
	if (strcmp(dirName, "..") == 0){
		
		//pop(head);
		//return;
		printf("Error: directory not empty\n");
		return;
		
	} else if (strcmp(dirName, ".") == 0){
		
		//return;
		printf("Error: cannot delete self\n");
	}
	
	//printf("\n%s", dirName);
	
	FAT32DirEntry tempDirEntry;
	FAT32DirEntryLong tempDirEntryLong;
	
	do {
		
		setFirstSectorOfCluster(bb, firstDataSector, current_cluster_number, &sectBytes);
		
		sectBytes *= bb->sector_size;
		
		fseek(in, sectBytes, SEEK_SET);
		
		// i is 16 because 512 / 64 = 16
		for (i = 0; i < 8; i++) {
			//getline
			
			tempName[0] = '\0';
			
			if( fread(&tempDirEntryLong, sizeof(FAT32DirEntryLong), 1, in ) ){
		
				if (fread(&tempDirEntry, sizeof(FAT32DirEntry), 1, in)){
					
					strcpy(tempName, tempDirEntry.dir_name);
					
				}
				
			}
			
			tempName[11] = '\0';
			
			if (strcmp(tempName, dirName) == 0){
				
				flagFoundDir = -1;
				if(tempDirEntry.dir_attr == 16){ //if dir_attr == 0x10 -> means is directory
					//printf("\nwsfefwfwef%s", dirName);
					flagFoundDir = 1;
					
					int tempClusNum = tempDirEntry.dir_fst_clus_HI*256 + tempDirEntry.dir_fst_clus_LO;
					
					//push(head, tempClusNum, tempName);
					//current_cluster_number = tempClusNum;
					entryClusNum = current_cluster_number;
					
					dirNameClusNum = tempClusNum;
					dirNameEntryNum = i;
					
					flagFoundDir = -3;
					break;
					
				}
				
			}
			
			// if getline[0] == 0 then no more entries, return;
			if (tempDirEntry.dir_name[0] == 0/*strcmp(tempName, "\0") == 0*/){
				
				flagFoundDir = -2;
				
			} 
			
		}
		current_cluster_number = fATTable[current_cluster_number];
	} while ( (current_cluster_number != 0x0FFFFFF8 && current_cluster_number != 0x0FFFFFFF 
				&& current_cluster_number != 0x00000000) && flagFoundDir != 1 && flagFoundDir > -2);
				
	if (flagFoundDir == 0 || flagFoundDir == -2){
		
		printf("Error: directory not found\n");
		
	}else if (flagFoundDir == -1) {
		
		printf("Error: %s is not a directory\n",  tempName);
		
	}else{
		
		setFirstSectorOfCluster(bb, firstDataSector, dirNameClusNum, &sectBytes);
		
		sectBytes *= bb->sector_size;
		
		fseek(in, sectBytes, SEEK_SET);
		
		tempName[0] = '\0';
		
		//read out the . and .. directories
		if (fread(&tempDirEntry, sizeof(FAT32DirEntry), 1, in)){
					
			strcpy(tempName, tempDirEntry.dir_name);
			
		}
		
		if (fread(&tempDirEntry, sizeof(FAT32DirEntry), 1, in)){
					
			strcpy(tempName, tempDirEntry.dir_name);
			
		}
		
		//read next entry
		if (fread(&tempDirEntry, sizeof(FAT32DirEntry), 1, in)){
					
			strcpy(tempName, tempDirEntry.dir_name);
			
		}
		
		tempName[11] = '\0';
		
		if (tempName[0] != 0){
			
			printf("Error: Directory is non-empty\n");
			return;
		}// else:
		
		//fseek(in, sectBytes, SEEK_SET);
		fclose(in);
		in = NULL;
		
		out = fopen(FILE_NAME, "r+b");
		
		fseek(out, sectBytes, SEEK_SET);
		
		char zeroChar = 0;
		char e5Char = 0xe5;
		
		//memset(&emptyEntry, 0, 32);
		//memset(&e5Entry, 0, 32);
		//memset((&e5Entry)+1, 0, 31);
		
		//delete . entry
		//fwrite(&emptyEntry, sizeof(emptyEntry), 1, out);
		
		for (int z = 0; z < 32; z++){
			
			fwrite(&zeroChar, sizeof(char), 1, out);
			
		}
		
		//delete .. entry
		//fwrite(&emptyEntry, sizeof(emptyEntry), 1, out);
		
		for (int z = 0; z < 32; z++){
			
			fwrite(&zeroChar, sizeof(char), 1, out);
			
		}
		
		
		fATTable[dirNameClusNum] = 0;
		writeFAT();
		
		//setFirstSectorOfCluster(bb, firstDataSector, head->cluster_number, &sectBytes);
		setFirstSectorOfCluster(bb, firstDataSector, entryClusNum, &sectBytes);
		sectBytes *= bb->sector_size;
		
		sectBytes += 64*dirNameEntryNum;
		
		fseek(out, sectBytes, SEEK_SET);
		
		//long entry
		for (int z = 0; z < 32; z++){
			
			fwrite(&zeroChar, sizeof(char), 1, out);
			
		}
		
		fwrite(&e5Char, sizeof(char), 1, out);
		
		//short entry
		for (int z = 0; z < 31; z++){
			
			fwrite(&zeroChar, sizeof(zeroChar), 1, out);
			
		}
		
		fclose(out);
		out == NULL;
		
		in = fopen(FILE_NAME, "rb");
		
	}
	
}

void rmFile(char* dirName){
	
	int current_cluster_number = head->cluster_number;
	
	int entryClusNum;
	
	int dirNameClusNum = -1;
	int dirNameEntryNum = -1;
	
	int i = 0;
	
	int sectBytes = 0;
	
	int flagFoundDir = 0;
	
	char tempName[12];
	tempName[0] = '\0';
	
	//printf("\n%s", dirName);
	
	processDirName(dirName); //dirName MUST BE 13+ char
	
	if (strcmp(dirName, "..") == 0){
		
		//pop(head);
		//return;
		
	} else if (strcmp(dirName, ".") == 0){
		
		//return;
		
	}
	
	//printf("\n%s", dirName);
	
	FAT32DirEntry tempDirEntry;
	FAT32DirEntryLong tempDirEntryLong;
	
	do {
		
		setFirstSectorOfCluster(bb, firstDataSector, current_cluster_number, &sectBytes);
		
		sectBytes *= bb->sector_size;
		
		fseek(in, sectBytes, SEEK_SET);
		
		// i is 16 because 512 / 64 = 16
		for (i = 0; i < 8; i++) {
			//getline
			
			tempName[0] = '\0';
			
			if( fread(&tempDirEntryLong, sizeof(FAT32DirEntryLong), 1, in ) ){
		
				if (fread(&tempDirEntry, sizeof(FAT32DirEntry), 1, in)){
					
					strcpy(tempName, tempDirEntry.dir_name);
					
				}
				
			}
			
			tempName[11] = '\0';
			
			if (strcmp(tempName, dirName) == 0){
				
				flagFoundDir = -1;
				if(tempDirEntry.dir_attr != 16){ //if dir_attr == 0x10 -> means is directory
					//printf("\nwsfefwfwef%s", dirName);
					flagFoundDir = 1;
					
					int tempClusNum = tempDirEntry.dir_fst_clus_HI*256 + tempDirEntry.dir_fst_clus_LO;
					
					//push(head, tempClusNum, tempName);
					//current_cluster_number = tempClusNum;
					
					entryClusNum = current_cluster_number;
					
					dirNameClusNum = tempClusNum;
					dirNameEntryNum = i;
					
					flagFoundDir = -3;
					break;
					
				}
				
			}
			
			// if getline[0] == 0 then no more entries, return;
			if (tempDirEntry.dir_name[0] == 0/*strcmp(tempName, "\0") == 0*/){
				
				flagFoundDir = -2;
				
			} 
			
		}
		current_cluster_number = fATTable[current_cluster_number];
	} while ( (current_cluster_number != 0x0FFFFFF8 && current_cluster_number != 0x0FFFFFFF 
				&& current_cluster_number != 0x00000000) && flagFoundDir != 1 && flagFoundDir > -2);
				
	if (flagFoundDir == 0 || flagFoundDir == -2){
		
		printf("Error: file not found\n");
		
	}else if (flagFoundDir == -1) {
		
		printf("Error: %s is a directory, not a file\n",  tempName);
		
	}else{
		
		struct StackNode* rmFileHead = newNode(-1, "clusterstakroot"); // this is root ie first node of cluster num
		struct StackNode* rmFileRootNode = rmFileHead;
		
		current_cluster_number = dirNameClusNum;
		
		int flagEndClus = 0;
		
		do {
			
			//updateStack
			push(&rmFileHead, current_cluster_number, "clusterstack");
			
			setFirstSectorOfCluster(bb, firstDataSector, current_cluster_number, &sectBytes);
			
			sectBytes *= bb->sector_size;
			
			fseek(in, sectBytes, SEEK_SET);
			
			// i is 16 because 512 / 64 = 16
			for (i = 0; i < 8; i++) {
				//getline
				
				tempName[0] = '\0';
				
				if( fread(&tempDirEntryLong, sizeof(FAT32DirEntryLong), 1, in ) ){
			
					if (fread(&tempDirEntry, sizeof(FAT32DirEntry), 1, in)){
						
						strcpy(tempName, tempDirEntry.dir_name);
						
					}
					
				}
				
				tempName[11] = '\0';
				
				// if getline[0] == 0 then no more entries, return;
				if (tempDirEntry.dir_name[0] == 0){
					
					flagEndClus = 1;
					break;
					
				} 
				
			}
			current_cluster_number = fATTable[current_cluster_number];
		} while ( (current_cluster_number != 0x0FFFFFF8 && current_cluster_number != 0x0FFFFFFF 
					&& current_cluster_number != 0x00000000) && flagEndClus != 1);		//reached end cluster?
		
		char zeroChar = 0;
		char e5Char = 0xe5;
		
		current_cluster_number = rmFileHead->cluster_number;
		
		fclose(in);
		in = NULL;
		
		out = fopen(FILE_NAME, "r+b");
		
		//delete entries in dataclusters
		while(rmFileHead != rmFileRootNode){
			
			setFirstSectorOfCluster(bb, firstDataSector, rmFileHead->cluster_number, &sectBytes);
			sectBytes *= bb->sector_size;
			
			fseek(out, sectBytes, SEEK_SET);
			
			for (int delCount = 0; delCount < 16; delCount++){
				//delete 32bytes
				//fwrite(&emptyEntry, sizeof(emptyEntry), 1, out);
				
				for (int z = 0; z < 32; z++){
			
					fwrite(&zeroChar, sizeof(char), 1, out);
					
				}
				
			}
			
			fATTable[rmFileHead->cluster_number] = 0;
			writeFAT();
			
			pop(&rmFileHead);
			
			
		}
		
		//delete stack
		//freeStack(rmFileRootNode, rmFileHead);
		
		setFirstSectorOfCluster(bb, firstDataSector, entryClusNum, &sectBytes);
		
		sectBytes *= bb->sector_size;
		
		sectBytes += 64*dirNameEntryNum;
		
		fseek(out, sectBytes, SEEK_SET);
		
		//long entry
		for (int z = 0; z < 32; z++){
			
			fwrite(&zeroChar, sizeof(char), 1, out);
			
		}
		
		fwrite(&e5Char, sizeof(char), 1, out);
		
		//short entry
		for (int z = 0; z < 31; z++){
			
			fwrite(&zeroChar, sizeof(zeroChar), 1, out);
			
		}
		
		fclose(out);
		out == NULL;
		
		in = fopen(FILE_NAME, "rb");
		
	}
	
}
