#include <stdio.h>
#include <stdlib.h>

int main() {
    FILE * in = fopen("fat32.img", "rb");
    unsigned int i, start_sector, length_sectors;
    
    fseek(in, 0x1BE, SEEK_SET); // go to partition table start
    
    for(i = 0; i < 4; i++) { // read all four entries
        printf("\nPartition entry %d: First byte %02Xn", i, fgetc(in));
        printf("\n  Partition start in CHS: %02X:%02X:%02Xn", fgetc(in), fgetc(in), fgetc(in));
        printf("\n  Partition type %02Xn", fgetc(in));
        printf("\n  Partition end in CHS: %02X:%02X:%02Xn", fgetc(in), fgetc(in), fgetc(in));
        
        fread(&start_sector, 4, 1, in);
        fread(&length_sectors, 4, 1, in);
        printf("\n  Relative LBA address %08X, %d sectors longn", start_sector, length_sectors);
    }
    
    fclose(in);
    return 0;
}