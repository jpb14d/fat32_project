//
//  shell_fat32.c
//  
//
//  Created by Doug Ott II on 4/23/18.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "FAT32BootBlock.c"
#include "FAT32DirEntry.c"
#include "FAT32DirEntryLong.c"
#include "Stack.c"

// cluster indicators
#define FREE_CLUSTER  0x00000000
#define EOC_1_CLUSTER  0x0FFFFFF8
#define EOC_2_CLUSTER  0x0FFFFFFF

int checkFileOpen(FILE*);
void setFirstSectorOfCluster(FAT32BootBlock*, unsigned int, unsigned int, unsigned int *);

int getBlockInfo (FILE*, FAT32BootBlock*);

int readFAT (FILE* in, FAT32BootBlock*, unsigned int*);

int processDirName(char*);	//caller must provide 13 byte array

int checkProcessDirName(char*);

int readRootClus (FILE*, FAT32BootBlock*, FAT32DirEntry*, FAT32DirEntryLong*, unsigned int firstSectRoot);

void doInfo(FAT32BootBlock*);

void ls(FILE*, FAT32BootBlock*, unsigned int*, unsigned int, struct StackNode*);

void cd(FILE*, FAT32BootBlock*, unsigned int*, unsigned int,  struct StackNode**, char*);

void doSize(FILE*, FAT32BootBlock*, unsigned int*, unsigned int, struct StackNode*, char*);

void mkdir();

int main() {
	//variables
	FILE * in;
	FAT32BootBlock* bb;
	unsigned int firstDataSector;
	unsigned int firstSectRoot;
	unsigned int* fATTable;
	unsigned int rootClusterNum;
	struct StackNode* head;
    struct StackNode* root;
	
	in = fopen("fat32.img", "rb");
	
	//check if Image opened
	if (!checkFileOpen(in)) {
		
		return 1;
		
	}
	
	//init bb
	bb = (FAT32BootBlock*)calloc(1, sizeof(FAT32BootBlock));
	
	//info()
	getBlockInfo(in, bb);
	rootClusterNum = bb->bpb_rootcluster;
	
	head = newNode(rootClusterNum, ""); // this is root ie first node
	root = head;
	
	//inside loop
	doInfo(bb);
	
	fATTable = (unsigned int *)calloc( (bb->bpb_FATz32*512/4), sizeof(unsigned int) );
	
	readFAT(in, bb, fATTable);
	
	firstDataSector = bb->reserved_sectors + (bb->number_of_fats * bb->bpb_FATz32);
	setFirstSectorOfCluster(bb, firstDataSector, rootClusterNum, &firstSectRoot);
	
	//TODO: inside loop
	ls(in, bb, fATTable, firstDataSector, head);
	
	//TODO: inside loop
	int tempClusterNum = rootClusterNum;
	char tempName[13];
	
	strcpy(tempName, "ReD.");
	
	//cd(in, bb, fATTable, firstDataSector, &head , tempName);
	
	strcpy(tempName, "f10");
	
	//TODO: inside loop
	//doSize(in, bb, fATTable, firstDataSector, head, tempName);
	
	free(fATTable);
	free(bb);
	fclose(in);
	return 0;
}

int checkFileOpen(FILE* in){
	
	if (in == NULL){
		
		printf("\nERROR: Could not open image.\n");
		return 0;
		
	}else{
		
		printf("\nImage opened.\n");
		return 1;
		
	}
	
}

void setFirstSectorOfCluster(FAT32BootBlock* bb, unsigned int firstDataSector, unsigned int n, unsigned int* firstSectorOfCluster){
	
	*firstSectorOfCluster = ((n-2)*bb->sectors_per_cluster) + firstDataSector;
	
}

int getBlockInfo(FILE* in, FAT32BootBlock* bb){
	
	fseek(in, 0, SEEK_SET);
	
	if (fread(bb, sizeof(FAT32BootBlock), 1, in) ){
		
		//read
		
	} else {
		
		printf("Error: Block metadata could not be read.");
		
	}
	
	
	
}

int readFAT(FILE* in, FAT32BootBlock* bb, unsigned int* fATTable){
	
	int numReserved = bb->reserved_sectors;
	int sectSize = bb->sector_size;
	int numFatEntries = bb->bpb_FATz32*512/4;
	
	fseek(in, numReserved*sectSize, SEEK_SET);
	
	if( fread(fATTable, sizeof(unsigned int), numFatEntries, in ) ){
		
		printf("0x%08x\n", fATTable[0]);
		
	}
	
}

int processDirName(char* dirName){
	
	int i = 0, dotPos = -1;
	int j = 0, k = 0;
	
	for (i = 0; dirName[i] != '\0'; i++){
		
		if (dirName[i] == '.'){
			
			if (dotPos == -1){
				
				dotPos = i;
				
			}else{
				
				dotPos = -2;
				
			}
			
		}
		
	}
	
	if (dotPos == -2){
		
		printf("Error: filename contains too many periods. Please try again.");
		return 1;
		
	}else if (i > 12){
		
		printf("Error: filename too long. Please try again.");
		return 1;	
		
	}else if (dotPos == -1 && i > 11){
		
		printf("Error: filename too long. Please try again.");
		return 1;
		
	}else if (dotPos == 0){
		
		printf("Error: filename cannot start with /'./'. Please try again.");
		return 1;
		
	}else if (dotPos != -1 && dotPos < i - 4){
		
		printf("Error:extension too long. Please try again.");
		return 1;
		
	}else if (dotPos == -1){
		
		for (j = i; j < 11; j++){
			
			dirName[j] = ' ';
			
		}
		
		dirName[j] = '\0';
		
	} else {
		
		char tempName[13];
		tempName[0] = '\0';
		
		for (j = 0, k = 0; j < dotPos; j++){
			
			tempName[k] = dirName[j];
			k++;
			
		}
		
		j++; // skip dot
		
		while (k <= 7){
			
			tempName[k++] = ' ';
			
		}
		
		while(dirName[j] != '\0' ){
			
			tempName[k++] = dirName[j++];
			
		}
		
		while (k <= 10){
			
			tempName[k++] = ' ';
			
		}
		
		tempName[k] = '\0';
		
		strcpy(dirName, tempName);
		
	}
	
	for (j = 0; j < 11; j++){
		
		dirName[j] = toupper(dirName[j]);
		
	}
	
	dirName[j] = '\0';
	
	
}

int checkProcessDirName(char* dirName){
	
	int i = 0, dotPos = -1;
	int j = 0, k = 0;
	
	for (i = 0; dirName[i] != '\0'; i++){
		
		if (dirName[i] == '.'){
			
			if (dotPos == -1){
				
				dotPos = i;
				
			}else{
				
				dotPos = -2;
				
			}
			
		}
		
		if(dirName[i] == 0){
			
			
			
		}
		
	}
	
	if (dotPos == -2){
		
		printf("Error: filename contains too many periods. Please try again.");
		return 1;
		
	}else if (i > 12){
		
		printf("Error: filename too long. Please try again.");
		return 1;	
		
	}else if (dotPos == -1 && i > 11){
		
		printf("Error: filename too long. Please try again.");
		return 1;
		
	}else if (dotPos == 0){
		
		printf("Error: filename cannot start with /'./'. Please try again.");
		return 1;
		
	}else if (dotPos != -1 && dotPos < i - 4){
		
		printf("Error:extension too long. Please try again.");
		return 1;
		
	}else if (dotPos == -1){
		
		for (j = i; j < 11; j++){
			
			dirName[j] = ' ';
			
		}
		
		dirName[j] = '\0';
		
	} else {
		
		char tempName[13];
		tempName[0] = '\0';
		
		for (j = 0, k = 0; j < dotPos; j++){
			
			tempName[k] = dirName[j];
			k++;
			
		}
		
		j++; // skip dot
		
		while (k <= 7){
			
			tempName[k++] = ' ';
			
		}
		
		while(dirName[j] != '\0' ){
			
			tempName[k++] = dirName[j++];
			
		}
		
		while (k <= 10){
			
			tempName[k++] = ' ';
			
		}
		
		tempName[k] = '\0';
		
		strcpy(dirName, tempName);
		
	}
	
	for (j = 0; j < 11; j++){
		
		dirName[j] = toupper(dirName[j]);
		
	}
	
	dirName[j] = '\0';
	
	
}

int readRootClus (FILE* in, FAT32BootBlock* bb, FAT32DirEntry* rootDirEntry, FAT32DirEntryLong* rootLongEntry, unsigned int firstSectRoot){
	
	int sectBytes = (firstSectRoot) * bb->sector_size;
	
	fseek(in, sectBytes, SEEK_SET);
	
	if( fread(rootLongEntry, sizeof(FAT32DirEntryLong), 1, in ) ){
		
		if (fread(rootDirEntry, sizeof(FAT32DirEntry), 1, in)){
			
			printf("\nDir Name: %s", rootDirEntry->dir_name);
			printf("\nDir Attr: %d", rootDirEntry->dir_attr);
			printf("\nDir Size: %d", rootDirEntry->dir_file_size);
		
			int temp = rootDirEntry->dir_fst_clus_HI*256 + rootDirEntry->dir_fst_clus_LO;
			//printf("0x%07x", temp);
			printf("\n0x%08x\n", temp);
			
			
		}
		
	}
	
	unsigned int tempSect;
	setFirstSectorOfCluster(bb, firstSectRoot, 16, &tempSect);
	
	printf("%d", tempSect);
	
	sectBytes = (tempSect) * bb->sector_size;
	
	fseek(in, sectBytes, SEEK_SET);
	
	if( fread(rootLongEntry, sizeof(FAT32DirEntryLong), 1, in ) ){
		
		if (fread(rootDirEntry, sizeof(FAT32DirEntry), 1, in)){
			
			printf("\nDir Name: %s", rootDirEntry->dir_name);
			printf("\nDir Attr: %d", rootDirEntry->dir_attr);
			printf("\nDir Size: %d", rootDirEntry->dir_file_size);
		
			int temp = rootDirEntry->dir_fst_clus_HI*256 + rootDirEntry->dir_fst_clus_LO;
			//printf("0x%07x", temp);
			printf("\n0x%08x", temp);
			
			
		}
		
	}
	
	
}

void doInfo(FAT32BootBlock* bb){
	
	printf("\nSector Size: %hu", bb->sector_size);
	printf("\nSectors Per Cluster: %u", bb->sectors_per_cluster);
	printf("\nReserved Sectors: %hu", bb->reserved_sectors);
	printf("\nNumber of FATs: %u", bb->number_of_fats);
	printf("\nRoot Dir Entries: %hu", bb->root_dir_entries);
	printf("\nTotal Sectors Short: %hu", bb->total_sectors_short);
	printf("\nMedia Descriptor: %u", bb->media_descriptor);
	printf("\nFAT Size Sectors: %hu", bb->fat_size_sectors);
	printf("\nSectors Per Track: %hu", bb->sectors_per_track);
	printf("\nNumber of Heads: %hu", bb->number_of_heads);
	printf("\nHidden Sectors: %u", bb->hidden_sectors);
	printf("\nTotal Sectors Long: %u", bb->total_sectors_long);
	printf("\nbpb_FATz32: %u", bb->bpb_FATz32);
	printf("\nbpb_extflags: %u", bb->bpb_extflags);
	printf("\nbpb_fsver: %u", bb->bpb_fsver);
	printf("\nbpb_rootcluster: %u", bb->bpb_rootcluster);
	printf("\nBoot Sector Signature: %d\n", bb->boot_sector_signature);
	
}

void ls(FILE* in, FAT32BootBlock* bb, unsigned int* fATTable, unsigned int firstDataSector, struct StackNode* head){
	
	int current_cluster_number = head->cluster_number;
	
	int i = 0;
	
	int sectBytes = 0;
	
	char tempName[12];
	tempName[0] = '\0';
	
	FAT32DirEntry tempDirEntry;
	FAT32DirEntryLong tempDirEntryLong;
	
	do {
		
		setFirstSectorOfCluster(bb, firstDataSector, current_cluster_number, &sectBytes);
		
		sectBytes *= bb->sector_size;
		
		fseek(in, sectBytes, SEEK_SET);
		
		// i is 16 because 512 / 64 = 16
		for (i = 0; i < 8; i++) {
			//getline
			
			tempName[0] = '\0';
			
			if( fread(&tempDirEntryLong, sizeof(FAT32DirEntryLong), 1, in ) ){
		
				if (fread(&tempDirEntry, sizeof(FAT32DirEntry), 1, in)){
					
					strcpy(tempName, tempDirEntry.dir_name);
					
				}
				
			}
			
			// if getline[0] == 0 then no more entries, return;
			if (strcmp(tempName, "\0") == 0){
				
				return;
				
			} 
			
			// print getline[0 - 11 byte]
			printf("\n%s\n", tempName);
			
		}
		current_cluster_number = fATTable[current_cluster_number];
	} while (current_cluster_number != 0x0FFFFFF8 || current_cluster_number != 0x0FFFFFFF 
				|| current_cluster_number != 0x00000000);
	
}

void cd(FILE* in, FAT32BootBlock* bb, unsigned int* fATTable, unsigned int firstDataSector, struct StackNode** head, char* dirName){
	
	int current_cluster_number = (*head)->cluster_number;
	
	int i = 0;
	
	int sectBytes = 0;
	
	int flagFoundDir = 0;
	
	char tempName[12];
	tempName[0] = '\0';
	
	//printf("\n%s", dirName);
	
	processDirName(dirName); //dirName MUST BE 13+ char
	
	//printf("\n%s", dirName);
	
	FAT32DirEntry tempDirEntry;
	FAT32DirEntryLong tempDirEntryLong;
	
	do {
		
		setFirstSectorOfCluster(bb, firstDataSector, current_cluster_number, &sectBytes);
		
		sectBytes *= bb->sector_size;
		
		fseek(in, sectBytes, SEEK_SET);
		
		// i is 16 because 512 / 64 = 16
		for (i = 0; i < 8; i++) {
			//getline
			
			tempName[0] = '\0';
			
			if( fread(&tempDirEntryLong, sizeof(FAT32DirEntryLong), 1, in ) ){
		
				if (fread(&tempDirEntry, sizeof(FAT32DirEntry), 1, in)){
					
					strcpy(tempName, tempDirEntry.dir_name);
					
				}
				
			}
			
			tempName[11] = '\0';
			
			if (strcmp(tempName, dirName) == 0){
				
				flagFoundDir = -1;
				if(tempDirEntry.dir_attr == 16){ //if dir_attr == 0x10 -> means is directory
					printf("\nwsfefwfwef%s", dirName);
					flagFoundDir = 1;
					//TODO: add nodes of linked list
					//new node
					//node->dirname = dirname
					//node->clusternum = formula thingy
					int tempClusNum = tempDirEntry.dir_fst_clus_HI*256 + tempDirEntry.dir_fst_clus_LO;
					
					push(head, tempClusNum, tempName);
					break;
					
				}
				
			}
			
			// if getline[0] == 0 then no more entries, return;
			if (strcmp(tempName, "\0") == 0){
				
				return;
				
			} 
			
		}
		current_cluster_number = fATTable[current_cluster_number];
	} while ( (current_cluster_number != 0x0FFFFFF8 || current_cluster_number != 0x0FFFFFFF 
				|| current_cluster_number != 0x00000000) && flagFoundDir != 1 );
				
	if (flagFoundDir == 0){
		
		printf("Error: directory not found");
		
	}else if (flagFoundDir == -1) {
		
		printf("Error: %s is not a directory",  tempName);
		
	}else{
		
		printf("\n%d  %s", (*head)->cluster_number, (*head)->dir_name);
		//printf("sweggwe");
		
	}
	
}

void doSize(FILE* in, FAT32BootBlock* bb, unsigned int* fATTable, unsigned int firstDataSector, struct StackNode* head, char* dirName){
	
	int current_cluster_number = head->cluster_number;
	
	int i = 0;
	
	int sectBytes = 0;
	
	int flagFoundDir = 0;
	
	char tempName[12];
	tempName[0] = '\0';
	
	//printf("\n%s", dirName);
	
	processDirName(dirName); //dirName MUST BE 13+ char
	
	printf("\n%s", dirName);
	
	FAT32DirEntry tempDirEntry;
	FAT32DirEntryLong tempDirEntryLong;
	
	do {
		
		setFirstSectorOfCluster(bb, firstDataSector, current_cluster_number, &sectBytes);
		
		sectBytes *= bb->sector_size;
		
		fseek(in, sectBytes, SEEK_SET);
		
		// i is 16 because 512 / 64 = 16
		for (i = 0; i < 8; i++) {
			//getline
			
			tempName[0] = '\0';
			
			if( fread(&tempDirEntryLong, sizeof(FAT32DirEntryLong), 1, in ) ){
		
				if (fread(&tempDirEntry, sizeof(FAT32DirEntry), 1, in)){
					
					strcpy(tempName, tempDirEntry.dir_name);
					
				}
				
			}
			
			tempName[11] = '\0';
			
			if (strcmp(tempName, dirName) == 0){
				
				printf("\nFile Size of %s is %d", dirName, tempDirEntry.dir_file_size);
				flagFoundDir = 1;
				
			}
			
			// if getline[0] == 0 then no more entries, return;
			if (strcmp(tempName, "\0") == 0){
				
				return;
				
			} 
			
		}
		current_cluster_number = fATTable[current_cluster_number];
	} while ( (current_cluster_number != 0x0FFFFFF8 || current_cluster_number != 0x0FFFFFFF 
				|| current_cluster_number != 0x00000000) && flagFoundDir != 1);
				
	if (flagFoundDir == 0){
		
		printf("Error: directory not found");
		
	}else{
		
		//TODO
		
	}
	
}

int findFirstFreeCluster(FAT32BootBlock* bb, unsigned int* fATTable) {
	int i = 0;
	int totalClusters = countOfClusters(bb);
	while(i < totalClusters) {
		if(((fATTable[i] & EOC_2_CLUSTER) | FREE_CLUSTER) == FREE_CLUSTER)
			break;
		i++;
	}
	return i;
}

int countOfClusters(FAT32BootBlock* bb) {
	return sectorsInDataRegion(bb) / bb->sectors_per_cluster;
}

int sectorsInDataRegion(FAT32BootBlock* bb) {
	int FATSz = bb->bpb_FATz32;
	int totalSectors = bb->total_sectors_long;
	//totalDataRegionSectors = TotSec - (bb->reserved_sectors + (bb->number_of_fats * FATSz) + rootDirSectorCount(bb));
}

int rootDirSectorCount(FAT32BootBlock* bb) {
	return (bb->root_dir_entries * 32) + (bb->sector_size - 1) / bb->sector_size ;
}

