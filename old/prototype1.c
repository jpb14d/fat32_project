//
//  shell_fat32.c
//  
//
//  Created by Doug Ott II on 4/23/18.
//

#include <stdio.h>
#include <stdlib.h>


typedef struct {
	unsigned char jmp[3];
	char oem[8];
	unsigned short sector_size;
	unsigned char sectors_per_cluster;
	unsigned short reserved_sectors;
	unsigned char number_of_fats;
	unsigned short root_dir_entries;
	unsigned short total_sectors_short; // if zero, later field is used
	unsigned char media_descriptor;
	unsigned short fat_size_sectors;
	unsigned short sectors_per_track;
	unsigned short number_of_heads;
	unsigned long hidden_sectors;
	unsigned long total_sectors_long;
	
	unsigned char drive_number;
	unsigned char current_head;
	unsigned char boot_signature;
	unsigned long volume_id;
	char volume_label[11];
	char fs_type[8];
	char boot_code[436];
	unsigned short boot_sector_signature;
} __attribute((packed)) FAT32BootBlock;

int main() {
	//struct FAT32BootBlock bpb;
	FILE * in = fopen("fat32.img", "rb");
	FAT32BootBlock bb;
	int i;
	
	if (in == NULL) {
		printf("\nERROR: Could not open image.\n");
		return 0;
	}
	else
		printf("\nImage opened.\n");
	
	fseek(in, 0, SEEK_SET);
	fread(&bb, sizeof(FAT32BootBlock), 1, in);
	
	//for(i=0; i<2; i++) {
	printf("\nSector Size: %d", bb.sector_size);
	printf("\nSectors Per Cluster: %d", bb.sectors_per_cluster);
	printf("\nReserved Sectors: %d", bb.reserved_sectors);
	printf("\nNumber of FATs: %d", bb.number_of_fats);
	printf("\nRoot Dir Entries: %d", bb.root_dir_entries);
	printf("\nTotal Sectors Short: %d", bb.total_sectors_short);
	printf("\nMedia Descriptor: %d", bb.media_descriptor);
	printf("\nFAT Size Sectors: %d", bb.fat_size_sectors);
	printf("\nSectors Per Track: %d", bb.sectors_per_track);
	printf("\nNumber of Heads: %d", bb.number_of_heads);
	printf("\nHidden Sectors: %d", bb.hidden_sectors);
	printf("\nTotal Sectors Long: %d", bb.total_sectors_long);
	printf("\nDrive Number: %d", bb.drive_number);
	printf("\nCurrent Head: %d", bb.current_head);
	printf("\nBoot Signature: %d", bb.boot_signature);
	printf("\nVolume ID: %d", bb.volume_id);
	printf("\nBoot Sector Signature: \n%d", bb.boot_sector_signature);
	//}
	
	fclose(in);
	return 0;
}



