// C program for linked list implementation of stack
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
 
// A structure to represent a stack
struct StackNode
{
    int cluster_number;
    char dir_name[12];
    struct StackNode* left;
    struct StackNode* right;
};
 
struct StackNode* newNode(int cluster_number, char * name)
{
    struct StackNode* stackNode = (struct StackNode*) malloc(sizeof(struct StackNode));
    stackNode->cluster_number = cluster_number;
    stackNode->left = NULL;
    stackNode->right = NULL;
    strcpy(stackNode->dir_name, name);
    return stackNode;
}

void push(struct StackNode** head, int cluster_number, char * name)
{
    struct StackNode* stackNode = newNode(cluster_number, name);
    stackNode->left = *head;
    (*head)->right = stackNode;
    *head = stackNode;
}
 
void pop(struct StackNode** head)
{
    if ((*head)->left == NULL)
        return;
    struct StackNode* temp = *head;
    *head = (*head)->left;
    (*head)->right = NULL;
    int popped = temp->cluster_number;
    free(temp);
 
    return;
}

void print(struct StackNode* root) {
    
    if (root == NULL){
        
        printf("Error: Stack is empty");
        return;
    }
    
    while (root != NULL) {
        printf("%s/", root->dir_name);
        root = root->right;
    }
    printf("\n");
}

void freeStack(struct StackNode* root, struct StackNode* head){
    
    while(head != root){
        
        pop(&head);
        
    }
    
    pop(&root);
    root = 0;
    
}

int main()
{
    // the list is doubly linked
    // head is the end of the list
    // root is the top
    // when a node gets added it becomes the new head, root never changes, head is constantly changing
    // his means the head is always the current directory
    
    struct StackNode* head = newNode(2, ""); // this is root ie first node
    struct StackNode* root = head;

    push(&head, 31, "dhgdfhdf");
    push(&head, 20, "pizza");
    push(&head, 30, "soda");
 
    print(root);
    pop(&head);
    pop(&head);
    push(&head, 45, "hola");
    print(root);
    pop(&head);
    push(&head, 20, "red");
    push(&head, 20, "green");
    print(root);
    freeStack(root, head);
    print(root);
    
 
    return 0;
}