Project 3

Members: Douglas Ott, Rishav Guha, Johnny Barnes

Division of Labor:

Douglas:
- Boot Sector Info
- find_empty_cluster()
- deprecated version of mkdir
- testing
- README

Rishav: 
- ls
- cd
- size
- rmdir
- rmfile
- mkdir

Johnny:
- cd
- open
- close
- read
- write

-------------------------------------------------

Contents of tar archive:

fat32fs.c			- shell for fat32 file system
FAT32BootBlock.c		- struct for boot block info
FAT32DirEntry.c			- struct for DirEntry info
FAT32DirEntryLong.c		- struct for DirEntryLong info
Stack.c				- struct to represent a stack
openFileTable.c			- struct to represent an open file
README.txt
Makefile

-------------------------------------------------

How to compile:

> make clean
> make
> ./fat32fs.out

-------------------------------------------------

Known bugs and unfinished portions of the project:

- creat fails to make a new file in the appropriate directory
- read fails to properly print the data from the location in memory
- write fails to properly insert string argument into the image 
- parser cant handle some special characters
- create and makedir can create dirs and files with the same names

-------------------------------------------------

Special considerations:

- files/dirs are printed in all caps, regardless
	of actual capitalization.
- periods between filename and file type
	are represented by a space when printed

