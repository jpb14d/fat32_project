#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
 
// A structure to represent a stack
struct openFileNode
{
    char mode;
    unsigned short first_cluster_lo;
    unsigned short first_cluster_hi;
    struct openFileNode* left;
    struct openFileNode* right;
};
 
struct openFileNode* newOpenFileNode(char mode, unsigned short first_cluster_lo, unsigned short first_cluster_hi)
{
    struct openFileNode* openFileNode = (struct openFileNode*) malloc(sizeof(struct openFileNode));
    openFileNode->mode = mode;
    openFileNode->first_cluster_lo = first_cluster_lo;
    openFileNode->first_cluster_hi = first_cluster_hi;
    openFileNode->left = NULL;
    openFileNode->right = NULL;
    return openFileNode;
}

void pushOpenFileNode(struct openFileNode** head, char mode, unsigned short first_cluster_lo, unsigned short first_cluster_hi)
{
    struct openFileNode* openFileNode = newOpenFileNode(mode, first_cluster_lo, first_cluster_hi);
    openFileNode->left = *head;
    (*head)->right = openFileNode;
    *head = openFileNode;
}


void popOpenFileNode(struct openFileNode** head)
{
    if ((*head)->left == NULL)
        return;
    struct openFileNode* temp = *head;
    *head = (*head)->left;
    (*head)->right = NULL;
    free(temp);
}


//returns 1 if found, 0 if not
int searchOpenFileTable(struct openFileNode** head, unsigned short file_cluster_lo, unsigned short file_cluster_hi) {
	struct openFileNode * temp = *head;
	while (((temp->first_cluster_lo != file_cluster_lo) || (temp->first_cluster_hi != file_cluster_hi))  && (temp->left != NULL))
	{
		temp = temp->left;
	}
    if ((temp->first_cluster_lo == file_cluster_lo) && (temp->first_cluster_hi == file_cluster_hi)) {
    	head = &temp;
        return 1;
    }
    if (temp->left == NULL) {
    	head = &temp;
        return 0;
    }
    head = &temp;
    return -1; // if this is returned then error within search function
}

//returns mode if found, 'y' if internal function error
char getOpenFileMode(struct openFileNode** head, unsigned short file_cluster_lo, unsigned short file_cluster_hi) {
	struct openFileNode * temp = *head;
	while (((temp->first_cluster_lo != file_cluster_lo) || (temp->first_cluster_hi != file_cluster_hi))  && (temp->left != NULL))
	{
		temp = temp->left;
	}
    if ((temp->first_cluster_lo == file_cluster_lo) && (temp->first_cluster_hi == file_cluster_hi)) {
    	head = &temp;
        return temp->mode;
    }
    else {
        return 'y';
    }
}

void removeOpenFile(struct openFileNode** head, unsigned short file_cluster_lo, unsigned short file_cluster_hi) {
	struct openFileNode * temp = *head;
    if ((temp->first_cluster_lo == file_cluster_lo) && (temp->first_cluster_hi == file_cluster_hi)) {
        popOpenFileNode(head);
        return;
    }
	while (((temp->first_cluster_lo != file_cluster_lo) || (temp->first_cluster_hi != file_cluster_hi))  && (temp->left != NULL))
	{
		temp = temp->left;
	}
	temp->left->right = temp->right;
	temp->right->left = temp->left;
	free(temp);
}

void printOpenFileTable(struct openFileNode* root) {
	char temp[12];
    while (root != NULL) {
        printf("[%d:%d, mode = %c] ", root->first_cluster_lo, root->first_cluster_hi, root->mode);
        root = root->right;
    }
    printf("\n");
}

void freeOpenFileTable(struct openFileNode** head, struct openFileNode* root){
    while(((*head)->first_cluster_lo != root->first_cluster_lo) && ((*head)->first_cluster_hi != root->first_cluster_hi)) {  
        popOpenFileNode(head);
    }
}

/*
void removeSpaces(char *temp, char *str)
{
    int count = 0;
 
    for (int i = 0; str[i]; i++)
        if (str[i] != ' ')
            temp[count++] = str[i]; 
    temp[count] = '\0';
}

int main() {
	
	struct openFileNode* openFileHead;
	struct openFileNode* openFileRoot;
	openFileHead = newOpenFileNode('x', 2, 0); // this is root ie first node
	openFileRoot = openFileHead;
	
	pushOpenFileNode(&openFileHead, 'x', 6, 12);
	pushOpenFileNode(&openFileHead, 'x', 14, 456);
	pushOpenFileNode(&openFileHead, 'x', 23, 57);
	pushOpenFileNode(&openFileHead, 'x', 75, 576);
    printOpenFileTable(openFileRoot);


    if (searchOpenFileTable(&openFileHead, 6, 12)) {
        printf("6 found\n");
    }
    else {
        printf("6 not found\n");
    }

    removeOpenFile(&openFileHead, 6, 12);
    printOpenFileTable(openFileRoot);

    if (searchOpenFileTable(&openFileHead, 6, 46)) {
        printf("6 found\n");
    }
    else {
        printf("6 not found\n");
    }

    removeOpenFile(&openFileHead, 75, 576);
	pushOpenFileNode(&openFileHead, 'w', 95, 4576);
    printOpenFileTable(openFileRoot);


    removeOpenFile(&openFileHead, 23, 57);
    printOpenFileTable(openFileRoot);

    removeOpenFile(&openFileHead, 14, 456);
    printOpenFileTable(openFileRoot);

    removeOpenFile(&openFileHead, 95, 4576);
    printOpenFileTable(openFileRoot);

    freeOpenFileTable(&openFileHead, openFileRoot);
	return 0;
}
*/