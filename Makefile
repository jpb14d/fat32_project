CC=gcc
CFLAGS=-std=c99
DEPS = fat32fs.c FAT32BootBlock.c FAT32DirEntry.c FAT32DirEntryLong.c Stack.c
OBJ = fat32fs.o

%.o: %.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

fat32fs.out: $(OBJ)
	gcc -o $@ $^ $(CFLAGS)
	
.PHONY: clean

clean:
	rm -f *.o *~ core *.out fat32fs