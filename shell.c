/******************************************************************
 * John Barnes
 * Project 1
 * COP 4610
 * ****************************************************************/


#include <stdlib.h>
#include <stdio.h>
#include <sys/wait.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/time.h>
#include <string.h>

void shell_loop(void);      // Main loop
char * shell_read_line(void);   // reads input
char ** shell_split_line(char * line);  // pareses and tokenizes input
int shell_execute(char ** args);    // executes commands

// Builtin function declarations
int shell_exit(char ** args);
int shell_info(char ** args);
int shell_ls(char ** args);
int shell_cd(char ** args);
int shell_size(char ** args);
int shell_creat(char ** args);
int shell_mkdir(char ** args);
int shell_rm(char ** args);
int shell_rmdir(char ** args);
int shell_open(char ** args);
int shell_close(char ** args);
int shell_read(char ** args);
int shell_write(char ** args);



// Main function
int main(int argc, char ** argv)
{
    shell_loop();
    return 0;
}

// main loop
void shell_loop(void)
{
  char * line;
  char ** args;
  int status;
  char * pUser = getenv("USER");
  char * pHost = getenv("HOSTNAME");
  char cDirectory[255];

    do {
        getcwd(cDirectory, sizeof(cDirectory));
        printf("%s >> ", cDirectory);
        line = shell_read_line();
        args = shell_split_line(line);
        if (args != NULL) {
            status = shell_execute(args);
        }

        free(line);
        free(args);
    } while (status);
}

// read line from stdin, returns stdin input
char * shell_read_line(void)
{
  int bufsize = 1024;
  int position = 0;
  char * buffer = calloc(bufsize, sizeof(char));
  int c;

    if (!buffer) {
        fprintf(stderr, "shell: error in memory allocation\n");
        exit(EXIT_FAILURE);
    }

    while (1) {
    // Read a character
    c = getchar();

    // If we hit EOF, replace it with a null character and return.
    if (c == EOF || c == '\n') {
        buffer[position] = '\0';
        return buffer;
    }
    else {
            buffer[position] = c;
        }
        position++;

        // If we have exceeded the buffer, reallocate.
        if (position >= bufsize) {
            bufsize += 1024;
            buffer = realloc(buffer, bufsize);
            if (!buffer) {
                fprintf(stderr, "shell: error in memory allocation\n");
                exit(EXIT_FAILURE);
            }
        }
    }
}

// splits input into '\0' terminate token strings, returns tokens ends with NULL
char ** shell_split_line(char * line)
{
    int bufsize = 64, position = 0;
    char ** tokens = calloc(bufsize, sizeof(char*));
    char * token = (char*) calloc(255, sizeof(char));
    int i = 0;
    int j = 0;
    int looper = 1;

    if (!tokens && !token) {
        fprintf(stderr, "Shell: error in memory allocation\n");
        exit(EXIT_FAILURE);
    }

    do {
        if (line[i] == ' ' || line[i] == '\t' || line[i] == '\r' || line[i] == '\a' || line[i] == '\n' || line[i] == '\0') {
            token[j] = '\0';
            tokens[position] = token;
            j = 0;
            token = (char*) calloc(255, sizeof(char));
            if (!token) {
                fprintf(stderr, "Shell: error in memory allocation\n");
                exit(EXIT_FAILURE);
            }
            position++;
            if (position >= bufsize) {
                bufsize += 64;
                tokens = realloc(tokens, bufsize * sizeof(char*));
                if (!tokens) {
                    fprintf(stderr, "Shell: error in memory allocation\n");
                    exit(EXIT_FAILURE);
                }
            }
            if (line[i] == '\0')
                looper = 0;
        }
        else {
            if (line[i] == '|' || line[i] == '<' || line[i] == '>' || line[i] == '&') {
                if (line[i - 1] != ' ') {
                    token[j] = '\0';
                    tokens[position] = token;
                    j = 0;
                    token = (char*) calloc(255, sizeof(char));
                    if (!token) {
                        fprintf(stderr, "Shell: error in memory allocation\n");
                        exit(EXIT_FAILURE);
                    }
                    position++;
                    if (position >= bufsize) {
                        bufsize += 64;
                        tokens = realloc(tokens, bufsize * sizeof(char*));
                        if (!tokens) {
                            fprintf(stderr, "Shell: error in memory allocation\n");
                            exit(EXIT_FAILURE);
                        }
                    }                    
                }
                                    
                token[j] = line[i];
                token[j + 1] = '\0';
                tokens[position] = token;
                j = 0;
                token = (char*) calloc(255, sizeof(char));
                if (!token) {
                    fprintf(stderr, "Shell: error in memory allocation\n");
                    exit(EXIT_FAILURE);
                }
                position++;
                if (position >= bufsize) {
                    bufsize += 64;
                    tokens = realloc(tokens, bufsize * sizeof(char*));
                    if (!tokens) {
                        fprintf(stderr, "Shell: error in memory allocation\n");
                        exit(EXIT_FAILURE);
                    }
                }
                if (line[i + 1] == ' ') {
                    i++;
                }
                if (line[i + 1] == '\0') {
                    looper = 0;
                }
            }
            else {
                token[j] = line[i];
                j++;
            }
        }
        i++;
    } while(looper);

    if (tokens[0][0] == '|' || tokens[0][0] == '<' || tokens[0][0] == '>' || tokens[0][0] == '&') {
        fprintf(stderr, "Shell: improper use of %s\n", tokens[0]);
            free(token);
            tokens = NULL;
            return tokens;
    }

    int synChecker = 0;
    for (int k = 0; k < position; k++) {
        if (tokens[k][0] == '|' || tokens[k][0] == '<' || tokens[k][0] == '>' || tokens[k][0] == '&')   {
            synChecker++;
        }
        else {
            synChecker = 0;
        }
        if (synChecker > 1) {
            fprintf(stderr, "Shell: improper use of %s\n", tokens[k]);
            free(token);
            tokens = NULL;
            return tokens;
        }
    }
    tokens[position] = NULL;

    for (int y = 0; y != position; y++) {
        if (tokens[y][0] == '$') {
            token = (char*) calloc(255, sizeof(char));
            for (int h = 1; tokens[y][h] != '\0'; h++) {
                token[h - 1] = tokens[y][h];
                token[h] = '\0';
            }
            tokens[y] = getenv(token);
        }
    }
    free(token);
    return tokens;
}

// execute shell builtin functions, if not the lanuches standalone applications
int shell_execute(char ** args)
{
    if ((args[0] == NULL) || (strcmp(args[0], "") == 0))
        return 1;
	if (strcmp(args[0], "exit") == 0)
		return (shell_exit(args));
	if (strcmp(args[0], "info") == 0)
		return (shell_info(args));
	if (strcmp(args[0], "ls") == 0)
		return (shell_ls(args));
	if (strcmp(args[0], "cd") == 0)
		return (shell_cd(args));
	if (strcmp(args[0], "size") == 0)
		return (shell_size(args));
	if (strcmp(args[0], "creat") == 0)
		return (shell_creat(args));
	if (strcmp(args[0], "mkdir") == 0)
		return (shell_mkdir(args));
	if (strcmp(args[0], "rm") == 0)
		return (shell_rm(args));
	if (strcmp(args[0], "rmdir") == 0)
		return (shell_rmdir(args));
	if (strcmp(args[0], "open") == 0)
		return (shell_open(args));
	if (strcmp(args[0], "close") == 0)
		return (shell_close(args));
	if (strcmp(args[0], "read") == 0)
		return (shell_read(args));
	if (strcmp(args[0], "write") == 0)
		return (shell_write(args));
	printf("Error, no command found: %s\n", args[0]);
	return 1;
}

/*** BUILTIN FUNCTIONS ***/

//builtin function exit
int shell_exit(char ** args)
{
    return 0;
}

//builtin function info
int shell_info(char ** args)
{
    return 1;
}

//builtin function ls
int shell_ls(char ** args)
{  
    return 1;
}

//builtin function cd
int shell_cd(char ** args)
{
    return 1;
}

//builtin function size
int shell_size(char ** args)
{
    return 1;
}

//builtin function creat
int shell_creat(char ** args)
{
    return 1;
}

//builtin function mkdir
int shell_mkdir(char ** args)
{
    return 1;
}

//builtin function rm
int shell_rm(char ** args)
{ 
    return 1;
}

//builtin function rmdir
int shell_rmdir(char ** args)
{
    return 1;
}

//builtin function open
int shell_open(char ** args)
{
    return 1;
}

//builtin function close
int shell_close(char ** args)
{
    return 1;
}

//builtin function read
int shell_read(char ** args)
{
    return 1;
}

//builtin function write
int shell_write(char ** args)
{
    return 1;
}


/*
• Make a directory structure like the FAT32DirectoryBlock (remember to reserve some
space for the long entry)
• Call an ls function ls(int current_cluster_number {should be the first_cluster_number of
a directory})
• Function looks up all directories inside the current directory ( fseek, and
i*FAT32DirectoryStructureCreatedByYou, where ‘i’ is a counter)
• Iterate the above step while the i*FAT32DirectoryStructureCreatedByYou < sector_size
• When that happens, lookup FAT[current_cluster_number]
• If FAT[current_cluster_number] != 0x0FFFFFF8 or 0x0FFFFFFF or 0x00000000, then
current_cluster_number = FAT[current_cluster_number]. Do step 3 - 5 by resetting i
• Else, break
*/
/*
void ls(int current_cluster_number) {
	do {
		// i is 16 because 512 / 32 = 16
		for (int i = 0; i < 16; i++) {
			//getline
			// if getline[0] == 0 then no more entries, return;
			// print getline[0 - 11 byte]
		}
		current_cluster_number = FAT[current_cluster_number];
	} while (current_cluster_number != 0x0FFFFFF8 or 0x0FFFFFFF or 0x00000000)
}
*/

/*
SEARCH FUNCTION
When given a file/folder name, returns the cluster number, or -1 if not found
int search(char * directory_name);
*/

/*
typedef struct {
	unsigned int cluster_number;
	pathNode * next;
} PathNode;

PathNode rootNode;
rootNode.cluster_number = bb->bpb_rootcluster;
rootNode->next = NULL;

PathNode * tempNode = malloc(PathNode);
if (rootNode->next == NULL)
	rootNode->next = tempNode;
else {
	PathNode * itrNode = &RootNode;
	while (itrNode->next != NULL)
		itrNode = itrNode->next;
}

typedef struct {
	unsigned int cluster_number;
	pathNode * next;
} PathNode;

PathNode * rootNode = NULL;
rootNode = malloc(sizeof(PathNode));
if (rootNode == NULL) {
    return 1;
}

rootNode->cluster_number = bb->bpb_rootcluster;
rootNode->next = NULL;


void push(node_t * head, int val) {
    node_t * current = head;
    while (current->next != NULL) {
        current = current->next;
    }

    // now we can add a new variable 
    current->next = malloc(sizeof(node_t));
    current->next->val = val;
    current->next->next = NULL;
}

void pop(node_t * head) {
    // if tonly root is left, do nothing
    if (head->next == NULL) {
        return;
    }

    // get to the second to last node in the list 
    node_t * current = head;
    while (current->next->next != NULL) {
        current = current->next;
    }

    // now current points to the second to last item of the list, so let's remove current->next ;
    free(current->next);
    current->next = NULL;

}

*/